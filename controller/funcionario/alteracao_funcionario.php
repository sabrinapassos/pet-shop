<?php

$id = $_GET["id"];
$func = $_GET["nome"];
$sexo = $_GET["sexo"];
$cpf = $_GET["cpf"];
$cpf = str_replace(".", "", $cpf);
$cpf = str_replace("-", "", $cpf);
$rg = $_GET["rg"];
$rg = str_replace(".", "", $rg);
$ec = $_GET["estado_civil"];
$funcao = $_GET["funcao"];
$log = $_GET["log"];
$bairro = $_GET["bairro"];
$comp = $_GET["complemento"];
$cep = $_GET["cep"];
$cep = str_replace("-", "", $cep);
$cidade = $_GET["cidades"];
$end = $_GET["endereco"];

if (!empty($_GET['tipo'])) {
    $tipotel = $_GET['tipo'];
}
if (!empty($_GET["ddd"])) {
    $ddd = $_GET["ddd"];
}
if (!empty($_GET["numTelefone"])) {
    $tel = $_GET["numTelefone"];
}

include '../../conectbd.php';
try {
    $query = $link->prepare("UPDATE tb_funcionario 
        SET nome_func = (:nome_func),
            sexo = (:sexo),
            cpf = (:cpf),
            rg = (:rg),
            estado_civil = (:estado_civil),
            fk_endereco  = (:endereco),
            funcao  = (:funcao)
        WHERE pk_funcionario = (:id)");

    $query->bindParam(':id', $id);
    $query->bindParam(':nome_func', $func);
    $query->bindParam(':sexo', $sexo);
    $query->bindParam(':cpf', $cpf);
    $query->bindParam(':rg', $rg);
    $query->bindParam(':estado_civil', $ec);
    $query->bindParam(':endereco', $end);
    $query->bindParam(':funcao', $funcao);
    $query->execute();

    $queryend = $link->prepare("select fk_endereco from tb_funcionario  where pk_funcionario=:id");
    $queryend->bindParam(':id', $id);
    $queryend->execute();

    $row = $queryend->fetch();
    if (!empty($row["fk_endereco"])) {
        $endereco = $row["fk_endereco"];
        $queryEndereco = $link->prepare("UPDATE tb_endereco SET logradouro = :logradouro, complemento = :complemento,
                bairro = :bairro, fk_cidade = :fk_cidade, cep = :cep WHERE pk_endereco=:id");
        $queryEndereco->bindParam(':logradouro', $log);
        $queryEndereco->bindParam(':complemento', $comp);
        $queryEndereco->bindParam(':bairro', $bairro);
        $queryEndereco->bindParam(':fk_cidade', $cidade);
        $queryEndereco->bindParam(':cep', $cep);
        $queryEndereco->bindParam(':id', $endereco);
        $queryEndereco->execute();
    }

    if (!empty($tel) && count($tel) > 0) {

        $queryTel = $link->prepare("delete from tb_telefone_funcionario  where fk_funcionario=:id");
        $queryTel->bindParam(':id', $id);
        $queryTel->execute();

        for ($i = 0; $i < count($tel); $i++) {

            $queryTelefone = $link->prepare("INSERT INTO tb_telefone_funcionario    
            (fk_funcionario, ddd, telefone, fk_tipo) 
	  values (:fk_funcionario, :ddd, :telefone, :fk_tipo)");

            $queryTelefone->bindParam(':fk_funcionario', $id);
            $queryTelefone->bindParam(':ddd', $ddd[$i]);
            $queryTelefone->bindParam(':telefone', $tel[$i]);
            $queryTelefone->bindParam(':fk_tipo', $tipotel[$i]);
            $queryTelefone->execute();
        }
    }

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Funcionario/funcionario_lista.php');
    
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}



?>
