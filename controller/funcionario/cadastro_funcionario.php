<?php
        $func = $_GET["nome"];
        $sexo = $_GET["sexo"];
        $cpf = $_GET["cpf"];
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $rg = $_GET["rg"];
        $rg = str_replace(".", "", $rg);
        $ec = $_GET["estado_civil"];
        $log = $_GET["log"];
        $bairro = $_GET["bairro"];
        $comp = $_GET["complemento"];
        $cep = $_GET["cep"];
        $cep = str_replace("-", "", $cep);
        $cidade = $_GET["cidades"];
        $funcao = $_GET["funcao"];
        
        if (!empty($_GET['tipo'])) {
            $tipotel = $_GET['tipo'];
        }
        if (!empty($_GET["ddd"])) {
            $ddd = $_GET["ddd"];
        }
        if (!empty($_GET["numTelefone"])) {
            $tel = $_GET["numTelefone"];
        }

        include '../../conectbd.php';
        try {
            $queryendereco = $link->prepare("INSERT INTO tb_endereco (logradouro,complemento,bairro,cep,fk_cidade)values(:logradouro,:complemento,:bairro,:cep,:fk_cidade)");

            $queryendereco->bindParam(':logradouro', $log);
            $queryendereco->bindParam(':complemento', $comp);
            $queryendereco->bindParam(':bairro', $bairro);
            $queryendereco->bindParam(':cep', $cep);
            $queryendereco->bindParam(':fk_cidade', $cidade);
            $queryendereco->execute();

            $end = $link->lastInsertId();
            $queryfuncionario = $link->prepare("INSERT INTO tb_funcionario (nome_func,sexo,cpf,rg,estado_civil,fk_endereco,funcao)values(:nome_func,:sexo,:cpf,:rg,:estado_civil,:endereco,:funcao)");

            $queryfuncionario->bindParam(':nome_func', $func);
            $queryfuncionario->bindParam(':sexo', $sexo);
            $queryfuncionario->bindParam(':cpf', $cpf);
            $queryfuncionario->bindParam(':rg', $rg);
            $queryfuncionario->bindParam(':estado_civil', $ec);
            $queryfuncionario->bindParam(':endereco', $end);
            $queryfuncionario->bindParam(':funcao', $funcao);
            $queryfuncionario->execute();


            $funcionario = $link->lastInsertId();
            if (!empty($tel) && count($tel) > 0) {
                for ($i = 0; $i < count($tel); $i++) {
                    $querytelefone = $link->prepare("INSERT INTO tb_telefone_funcionario (fk_funcionario,ddd,telefone,fk_tipo)values(:fk_funcionario,:ddd,:telefone,:fk_tipo)");

                    $querytelefone->bindParam(':fk_funcionario', $funcionario);
                    $querytelefone->bindParam(':ddd', $ddd[$i]);
                    $querytelefone->bindParam(':telefone', $tel[$i]);
                    $querytelefone->bindParam(':fk_tipo', $tipotel[$i]);
                    $querytelefone->execute();
                }
            }


            header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Funcionario/funcionario_lista.php');
        } catch (Exception $e) {
            echo "Falha na inclusão" . $e->getMessage();
        }
        ?>
  
