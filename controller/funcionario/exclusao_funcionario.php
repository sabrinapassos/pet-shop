<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<!DOCTYPE html>
<!-- cadastro.html -->
<html>
    <head>
        <title>Cadastro de Funcionario - Exclusão</title>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
        $id = $_GET["id"];

        include '../../conectbd.php';

        try {
            $queryTel = $link->prepare("delete from tb_telefone_funcionario  where fk_funcionario =:id");

            $queryTel->bindParam(':id', $id);
            $queryTel->execute();
            
            $query = $link->prepare("delete from tb_funcionario  where pk_funcionario =:id");

            $query->bindParam(':id', $id);
            $query->execute();

            header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Funcionario/funcionario_lista.php');
        
        } catch (PDOException $e) {
            echo "Falha na exclusão: " . $e->getMessage();
        }
        
        ?>  
        <br>

    </body>
</html>
