<?php

$id = $_GET["id"];
$qtd = $_GET["qtd_prod"];
$preco = $_GET["preco_prod"];
$desc = $_GET["descricao_prod"];

include '../../conectbd.php';
try {
    $query = $link->prepare("UPDATE tb_produto 
      SET qtd_prod = (:qtd_prod),
          preco_prod = (:preco_prod),
          descricao_prod = (:descricao_prod)    
	  WHERE pk_produto = (:id)");

    $query->bindParam(':id', $id);
    $query->bindParam(':qtd_prod', $qtd);
    $query->bindParam(':preco_prod', $preco);
    $query->bindParam(':descricao_prod', $desc);
    $query->execute();

    echo "Alterado com precisão.";
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}

header('Location: http://localhost/pet-shop-workspace/pet-shop/view/produto/produto_lista.php');
?>

