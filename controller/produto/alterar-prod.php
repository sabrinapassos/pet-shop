
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
$valor = urldecode($_GET["preco"]);
$id = urldecode($_GET["id"]);
$qtd = urldecode($_GET["qtd"]);
$preco = str_replace(",", ".", $valor);
$desc = urldecode($_GET["desc"]);
?>
<html>
    <head>
        <title>Cadastro de Produtos</title>

        <script src="../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css"/>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <script>
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
    </script>

    <body style="font-size: 12px">

        <?php include 'menu.php' ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="alteracao-produto.php">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Produtos</h2>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="qtd_prod">Quantidade</label>  
                        <div class="col-md-4">
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <input id="textinput" name="qtd_prod" type="number" value="<?php echo $qtd; ?>" class="form-control input-md"/>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="preco_prod">Preço</label>  
                        <div class="col-md-4">
                            <input id="textinput" name="preco_prod" type="number" value="<?php echo $preco ?>" class="form-control input-md"  size="20" maxlength="10" class="money" />
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="descricao_prod">Descrição</label>  
                        <div class="col-md-4">
                            <textarea id="textinput" name="descricao_prod"    cols="40" class="form-control input-md"><?php echo $desc ?></textarea>
                        </div>
                    </div>

            </div>
            <div class="form-group">
                <div class="col-md-5">&nbsp;</div>
                <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Salvar"/>

                <a href="produto_lista.php"><input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/></a>
            </div>


        </div>

    </fieldset>
</form>
</div>
</div>
</body>
</html>

