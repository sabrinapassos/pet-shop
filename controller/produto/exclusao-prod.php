<?php

session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>

<?php

$id = $_GET["id"];

include '../../conectbd.php';

try {
    $query = $link->prepare("delete from tb_produto where pk_produto=:id");

    $query->bindParam(':id', $id);
    $query->execute();

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/produto/produto_lista.php');

} catch (PDOException $e) {
    echo "Falha na exclusão: " . $e->getMessage();
}
?>  
