<?php
$valor = $_GET["preco_prod"];
$preco = str_replace(".", "", $valor);
$preco = str_replace(",", ".", $preco);
$desc = $_GET["descricao_prod"];

include '../../conectbd.php';
try {
    $query = $link->prepare("INSERT INTO tb_produto 
      (preco_prod, descricao_prod) 
	  values (:preco_prod, :descricao_prod)");

    $query->bindParam(':preco_prod', $preco);
    $query->bindParam(':descricao_prod', $desc);
    $query->execute();

    //echo"<script language='javascript' type='text/javascript'> window.open(' ../../view/produto/produto_lista.php', '_self');</script>";

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/produto/produto_lista.php');
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}
?>  
