<?php

session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>

<?php

if (!empty($_GET["id"])) {
    $id = $_GET["id"];
}

$valor = $_GET["preco"];
$servico = $_GET["servico"];
$preco = str_replace(".", "", $valor);
$preco = str_replace(",", ".", $preco);

include '../../conectbd.php';
try {
    if (!empty($id)) {
        $query = $link->prepare("UPDATE tb_servico SET servico = :servico, preco = :preco"
                . "where pk_servico = :pk_servico");

        $query->bindParam(':pk_servico', $id);
        $query->bindParam(':servico', $servico);
        $query->bindParam(':preco', $preco);
        $query->execute();
    } else {
        $query = $link->prepare("INSERT INTO tb_servico (servico, preco) values (:servico, :preco)");

        $query->bindParam(':servico', $servico);
        $query->bindParam(':preco', $preco);
        $query->execute();
    }

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/servico/servico_lista.php');
    
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}
?>  


