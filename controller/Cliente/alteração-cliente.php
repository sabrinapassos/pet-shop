<?php

$id = $_GET["id"];


/* Cliente */
$cliente = $_GET["nomecliente"];
$cpf = $_GET["cpf"];
$cpf = str_replace(".", "", $cpf);
$cpf = str_replace("-", "", $cpf);
$rg = $_GET["rg"];
$rg = str_replace(".", "", $rg);
$email = $_GET["email"];
$dtnasc = $_GET["datanasc"];
/* Endereço */
$cep = $_GET["cep"];
$cep = $_GET["cpf"];
$cep = str_replace(".", "", $cep);
$cep = str_replace("-", "", $cep);
$log = $_GET["log"];
$bairro = $_GET["bairro"];
$complemento = $_GET["complemento"];
$uf = $_GET["uf"];
$cidade = $_GET["cidades"];
/* Telefone */
if (!empty($_GET['tipo'])) {
    $tipo = array($_GET['tipo']);
}
if (!empty($_GET['ddd'])) {
    $ddd = array($_GET['ddd']);
}
if (!empty($_GET['telefone'])) {
    $numtel = array($_GET['telefone']);
}

include '../../conectbd.php';
try {
    $querycliente = $link->prepare("UPDATE tb_cliente
    SET nome_cliente =:cliente, cpf=:cpf, rg=:rg, email=:email, datanasc=:datanasc
    WHERE pk_cliente= :pk_cliente;");

    $querycliente->bindParam(':cliente', $cliente);
    $querycliente->bindParam(':cpf', $cpf);
    $querycliente->bindParam(':rg', $rg);
    $querycliente->bindParam(':email', $email);
    $querycliente->bindParam(':datanasc', $dtnasc);
    $querycliente->bindParam('pk_cliente', $id);
    $querycliente->execute();

    $queryEndereco = $link->prepare("SELECT fk_endereco FROM tb_cliente WHERE pk_cliente=:id");
    $queryEndereco->bindParam(':id', $id);
    $queryEndereco->execute();

    $row = $queryEndereco->fetch();
    if (!empty($row["fk_endereco"])) {
        $endereco = $row["fk_endereco"];
        $queryEndereco = $link->prepare("UPDATE tb_endereco SET logradouro = :logradouro, complemento = :complemento,
                bairro = :bairro, fk_cidade = :fk_cidade, cep = :cep WHERE pk_endereco=:id");
        $queryEndereco->bindParam(':logradouro', $log);
        $queryEndereco->bindParam(':complemento', $complemento);
        $queryEndereco->bindParam(':bairro', $bairro);
        $queryEndereco->bindParam(':fk_cidade', $cidade);
        $queryEndereco->bindParam(':cep', $cep);
        $queryEndereco->bindParam(':id', $endereco);
        $queryEndereco->execute();
    }

    if (!empty($numTel) && count($numTel) > 0) {

        $queryTel = $link->prepare("delete from tb_telefone_cliente where fk_cliente=:id");
        $queryTel->bindParam(':id', $id);
        $queryTel->execute();

        for ($i = 0; $i < count($numTel); $i++) {

            $queryTelefone = $link->prepare("INSERT INTO tb_telefone_cliente    
            (fk_cliente, ddd, telefone, fk_tipo) 
	  values (:fk_cliente, :ddd, :telefone, :fk_tipo)");

            $queryTelefone->bindParam(':fk_cliente', $id);
            $queryTelefone->bindParam(':ddd', $ddd[$i]);
            $queryTelefone->bindParam(':telefone', $numTel[$i]);
            $queryTelefone->bindParam(':fk_tipo', $selectTipo[$i]);
            $queryTelefone->execute();
        }
    }

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Cliente/cliente_lista.php');
    
} catch (PDOException $e) {
    echo "Falha na inclusão:" . $e->getMessage();
}
?>