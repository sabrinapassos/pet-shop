<?php

/* Cliente */
$nome = $_GET["nomecliente"];
$cpf = $_GET["cpf"];
$cpf = str_replace(".", "", $cpf);
$cpf = str_replace("-", "", $cpf);
$rg = $_GET["rg"];
$rg = str_replace(".", "", $rg);
$email = $_GET["email"];
$dtnasc = $_GET["datanasc"];

/* Endereço */
$cep = $_GET["cep"];
$cep = str_replace("-", "", $cep);
$log = $_GET["log"];
$bairro = $_GET["bairro"];
$complemento = $_GET["complemento"];
$uf = $_GET["uf"];
$cidade = $_GET["cidades"];
echo "$cidade";

/* Telefone */
if (!empty($_GET['tipo'])) {
    $tipo = array($_GET['tipo']);
}
if (!empty($_GET['ddd'])) {
    $ddd = array($_GET['ddd']);
}
if (!empty($_GET['telefone'])) {
    $numtel = array($_GET['telefone']);
}
include '../../conectbd.php';
try {
    $queryendereco = $link->prepare("INSERT INTO tb_endereco (logradouro,complemento,bairro,cep,fk_cidade)values(:logradouro,:complemento,:bairro,:cep,:fk_cidade)");

    $queryendereco->bindParam(':logradouro', $log);
    $queryendereco->bindParam(':complemento', $complemento);
    $queryendereco->bindParam(':bairro', $bairro);
    $queryendereco->bindParam(':fk_cidade', $cidade);
    $queryendereco->bindParam(':cep', $cep);
    $queryendereco->execute();

    $end = $link->lastInsertid();
    $queryclient = $link->prepare("INSERT INTO tb_cliente(nome_cliente,cpf,rg,email,datanasc,fk_endereco) values (:cliente,:cpf,:rg,:email,:datanasc,:fk_endereco)");

    $queryclient->bindParam(':cliente', $nome);
    $queryclient->bindParam(':cpf', $cpf);
    $queryclient->bindParam(':rg', $rg);
    $queryclient->bindParam(':email', $email);
    $queryclient->bindParam(':datanasc', $dtnasc);
    $queryclient->bindParam(':fk_endereco', $end);
    $queryclient->execute();

    $cliente = $link->lastInsertId();

    if (!empty($numtel) && count($numtel) > 0) {
        for ($i = 0; $i < count($numtel); $i++) {
            $querytelefone = $link->prepare("INSERT INTO tb_telefone_cliente(ddd,telefone,fk_cliente,fk_tipo)"
                    . "values(:ddd,:telefone,:fk_cliente,:fk_tipo)");

            $querytelefone->bindParam(':ddd', $ddd[$i]);
            $querytelefone->bindParam(':telefone', $numtel[$i]);
            $querytelefone->bindParam(':fk_tipo', $tipo[$i]);
            $querytelefone->bindParam(':fk_cliente', $cliente);
            $querytelefone->execute();
        }
    }


    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Cliente/cliente_lista.php');
} catch (PDOException $e) {
    echo "Falha na inclusão:" . $e->getMessage();
}
?>