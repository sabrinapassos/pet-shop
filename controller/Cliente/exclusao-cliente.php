<?php

$id = $_GET["id"];

include '../../conectbd.php';

try {
    $queryEndereco = $link->prepare("SELECT fk_endereco FROM tb_cliente WHERE pk_cliente=:id");
    $queryEndereco->bindParam(':id', $id);
    $queryEndereco->execute();

    $row = $queryEndereco->fetch();
    if (!empty($row["fk_endereco"])) {
        $end = $row["pk_cliente"];
        $queryEndereco = $link->prepare("DELETE FROM tb_endereco WHERE pk_endereco=:id");
        $queryEndereco->bindParam(':id', $end);
        $queryEndereco->execute();
    }

    $querytelefone = $link->prepare("delete from tb_telefone_cliente where pk_telefone_cliente=:id");
    $querytelefone->bindParam(':id', $id);
    $querytelefone->execute();

    $queryclient = $link->prepare("delete from tb_cliente where pk_cliente=:id");
    $queryclient->bindParam(':id', $id);
    $queryclient->execute();

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Cliente/cliente_lista.php');
    
} catch (PDOException $ex) {
    echo "Falha na exclusão: " . $ex->getMessage();
}
?>
