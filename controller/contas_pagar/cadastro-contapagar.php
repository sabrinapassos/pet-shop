<?php

$fornecedor = $_GET["fornecedor"];
$compra = $_GET["compra"];
$code = $_GET["codebarra"];
$valortotal=$_GET["valorconta"];
$valortotal = str_replace(".", "", $valortotal);
$valortotal = str_replace(",", ".", $valortotal);
$nossonumero=$_GET["nossonumero"];
$totalparc=$_GET["totalparc"];
$parcela=$_GET["numparc"];
$datavenc =$_GET["datadevencimento"];
$pago =$_GET["valorpago"];
$datapag =$_GET["datadepagamento"];

include '../../conectbd.php';

try {
    $query = $link->prepare
            ("INSERT INTO tb_conta_pagar (fk_fornecedor, fk_compra, total_parc, num_parc, nosso_num, dt_vencimento, vl_conta, cod_barra, dt_pag, vl_pag)
                                  values(:fk_fornecedor,:fk_compra,:totalparc, :numparc, :nossonumero, :datadevencimento,:valorconta, :codebarra, :datadepagamento, :valorpago)");

    $query->bindParam(':fk_fornecedor', $fornecedor);
    $query->bindParam(':fk_compra', $compra);
    $query->bindParam(':totalparc', $totalparc);
    $query->bindParam(':numparc', $parcela);
    $query->bindParam(':nossonumero', $nossonumero);
    
    $query->bindParam(':datadevencimento', $datavenc);
    $query->bindParam(':valorconta', $valortotal);
    $query->bindParam(':codebarra', $code);
    $query->bindParam(':datadepagamento', $datapag);
    $query->bindParam(':valorpago', $pago);

    $query->execute();

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Contas_a_pagar/Contas_pagar_lista.php');
   
} catch (Exception $e) {
    echo "Falha na inclusão" . $e->getMessage();
}
?>
