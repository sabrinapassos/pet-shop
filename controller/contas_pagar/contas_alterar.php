<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<?php

$id = $_GET["id"];


$fornecedor = $_GET["fornecedor"];
$compra = $_GET["compra"];
$code = $_GET["codebarra"];
$valortotal=$_GET["valorconta"];
$valortotal = str_replace(".", "", $valortotal);
$valortotal = str_replace(",", ".", $valortotal);
$nossonumero=$_GET["nossonumero"];
$totalparc=$_GET["totalparc"];
$parcela=$_GET["numparc"];
$datavenc =$_GET["datadevencimento"];
$pago =$_GET["valorpago"];
$datapag =$_GET["datadepagamento"];

include '../../conectbd.php';

try {
    
    $query = $link->prepare
            ("UPDATE tb_conta_pagar SET fk_fornecedor=:fk_fornecedor, fk_compra=:fk_compra, total_parc=:totalparc, num_parc=:numparc, nosso_num=:nossonumero, dt_vencimento=:datadevencimento, vl_conta=:valorconta, cod_barra=:codebarra, dt_pag=:datadepagamento, vl_pag=:valorpago)
              WHERE pk_conta_pagar=:pk_conta_pagar" );

    $query->bindParam(':fk_fornecedor', $fornecedor);
    $query->bindParam(':fk_compra', $compra);
    $query->bindParam(':totalparc', $totalparc);
    $query->bindParam(':numparc', $parcela);
    $query->bindParam(':nossonumero', $nossonumero);
    
    $query->bindParam(':datadevencimento', $datavenc);
    $query->bindParam(':valorconta', $valortotal);
    $query->bindParam(':codebarra', $code);
    $query->bindParam(':datadepagamento', $datapag);
    $query->bindParam(':valorpago', $pago);

    $query->execute();



    echo "<h1>cadastro realizado com sucesso.</h1>";

    echo"<script language='javascript' type='text/javascript'> window.open('../../view/Contas_a_pagar/Contas_pagar_lista.php', '_self');</script>";
//            header('Location:../../view/Contas_a_pagar/Contas_pagar_lista.php');
} catch (Exception $e) {
    echo "Falha na inclusão" . $e->getMessage();
}
?>
