<?php

$id = $_GET["id"];

include '../../conectbd.php';

try {

    $queryclient = $link->prepare("delete from tb_conta_pagar where pk_conta_pagar=:id");
    $queryclient->bindParam(':id', $id);
    $queryclient->execute();

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Contas_a_pagar/Contas_pagar_lista.php');
   
} catch (PDOException $ex) {
    echo "Falha na exclusão: " . $ex->getMessage();
}
?>
