<?php

$id = $_GET["id"];

include '../../conectbd.php';

try {
    $querycta = $link->prepare("delete from tb_conta_pagar  where fk_compra =:id");

    $querycta->bindParam(':id', $id);
    $querycta->execute();


    $query = $link->prepare("delete from tb_compra  where pk_compra =:id");

    $query->bindParam(':id', $id);
    $query->execute();

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/compra/compra_lista.php');
    
} catch (PDOException $e) {
    echo "Falha na exclusão: " . $e->getMessage();
}
?>  
