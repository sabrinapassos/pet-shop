<?php

$fornecedor = $_GET["fornecedor"];
$valor = $_GET["valor_compra"];
$valor = str_replace(".", "", $valor);
$valor = str_replace(",", ".", $valor);


if (!empty($_GET['observacoes'])) {
    $obs = $_GET["observacoes"];
}

if (!empty($_GET['produtoId'])) {
    $produtoId = $_GET["produtoId"];
}

if (!empty($_GET['qtdProd'])) {
    $qtdProd = $_GET['qtdProd'];
}

if (!empty($_GET['vlUni'])) {
    $vlUni = $_GET['vlUni'];
    $vlUni = str_replace(".", "", $vlUni);
    $vlUni = str_replace(",", ".", $vlUni);
}

include '../../conectbd.php';
try {
    $query = $link->prepare("INSERT INTO tb_compra (fk_fornecedor,valor_compra, observacoes)values(:fk_fornecedor,:valor_compra,:observacoes)");

    $query->bindParam(':fk_fornecedor', $fornecedor);
    $query->bindParam(':valor_compra', $valor);
    $query->bindParam(':observacoes', $obs);
    $query->execute();

    $compra = $link->lastInsertId();

    if (!empty($produtoId) && count($produtoId) > 0) {
        for ($i = 0; $i < count($qtdProd); $i++) {

            $queryProd = $link->prepare("INSERT INTO tb_compra_produto (fk_compra, fk_produto, qtd_prod, vl_uni) 
                values (:fk_compra, :fk_produto, :qtd_prod, :vl_uni)");

            $queryProd->bindParam(':fk_compra', $compra);
            $queryProd->bindParam(':fk_produto', $produtoId[$i]);
            $queryProd->bindParam(':qtd_prod', $qtdProd[$i]);
            $queryProd->bindParam(':vl_uni', $vlUni[$i]);
            $queryProd->execute();


            $query2 = $link->prepare("select qtd_prod from tb_produto where pk_produto = :pk_produto");
            $query2->bindParam(':pk_produto', $produtoId[$i]);
            $query2->execute();

            $row = $query2->fetch();

            $qtdProduto = $row["qtd_prod"];
            $qtdProduto = $qtdProduto + $qtdProd[$i];

            $queryProd = $link->prepare("update tb_produto set qtd_prod = :qtd, preco_compra = :preco_compra where pk_produto = :pk_produto");
            $queryProd->bindParam(':pk_produto', $produtoId[$i]);
            $queryProd->bindParam(':qtd', $qtdProduto);
            $queryProd->bindParam(':preco_compra', $vlUni[$i]);
            $queryProd->execute();
        }
    }

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/compra/compra_lista.php');
    
} catch (Exception $e) {
    echo "Falha na inclusão" . $e->getMessage();
}
?>
