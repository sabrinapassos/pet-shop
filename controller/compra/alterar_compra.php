<?php

$fornecedor = $_GET["fornecedor"];
$produto = $_GET["produto"];
$quantidade = $_GET["quantidade"];
$valor = $_GET["valor_compra"];
$valor = str_replace(".", "", $valor);
$valor = str_replace(",", ".", $valor);
$desconto = $_GET["vl_desconto"];
$desconto = str_replace(".", "", $desconto);
$desconto = str_replace(",", ".", $desconto);
$acrescimo = $_GET["vl_ascre"];
$acrescimo = str_replace(".", "", $acrescimo);
$acrescimo = str_replace(",", ".", $acrescimo);
$obs = $_GET["observacoes"];

include '../../conectbd.php';
try {
    $query = $link->prepare("update tb_compra
                     SET fk_fornecedor = (:fk_fornecedor),
                     fk_produto = (:fk_produto),
                     quantidade = (:quantidade),
                     valor_compra = (:valor_compra),
                     observacoes = (:observacoes),
                     vl_desconto = (:vl_desconto),
                     vl_ascre = (:vl_ascre)
                     WHERE pk_compra = (:id)");

    $query->bindParam(':fk_fornecedor', $fornecedor);
    $query->bindParam(':fk_produto', $produto);
    $query->bindParam(':quantidade', $quantidade);
    $query->bindParam(':valor_compra', $valor);
    $query->bindParam(':observacoes', $obs);
    $query->bindParam(':vl_desconto', $desconto);
    $query->bindParam(':vl_ascre', $acrescimo);
    $query->execute();
    
    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/compra/compra_lista.php');
    
} catch (Exception $e) {
    echo "Falha na inclusão" . $e->getMessage();
}
?>
 

