<?php

$id = $_GET["id"];
$pet = $_GET["nomepet"];
$raca = $_GET["raca"];
$sexo = $_GET["sexo"];
$porte = $_GET["porte"];
$data = $_GET["data"];
$obs = $_GET["obs"];
$dono = $_GET["dono"];

include '../../conectbd.php';
try {
    $query = $link->prepare("UPDATE tb_pet 
        SET nome = (:nome),
            raca = (:raca),
            sexo = (:sexo),
            porte = (:porte),
            dt_nascimento = (:data),
            observacoes = (:obs),
            fk_cliente = (:fk_cliente)
        WHERE pk_pet = (:id)");

    $query->bindParam(':id', $id);
    $query->bindParam(':nome', $pet);
    $query->bindParam(':raca', $raca);
    $query->bindParam(':sexo', $sexo);
    $query->bindParam(':porte', $porte);
    $query->bindParam(':data', $data);
    $query->bindParam(':obs', $obs);
    $query->bindParam(':fk_cliente', $dono);
    $query->execute();

    echo "Alterado com precisão.";
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}

header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Pet/pet_lista.php');

?>


