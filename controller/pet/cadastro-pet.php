<?php
        $pet = $_GET["nomepet"];
        $raca = $_GET["raca"];
        $sexo = $_GET["sexo"];
        $porte = $_GET["porte"];
        $data = $_GET["data"];
        $obs = $_GET["obs"];
        $dono = $_GET["dono"];

        include '../../conectbd.php';
        try {
            $query = $link->prepare("INSERT INTO tb_pet(nome_pet,raca,sexo,porte,dt_nascimento,observacoes, fk_cliente)
                        values(:nome,:raca,:sexo,:porte,:data,:obs, :fk_cliente)");

            $query->bindParam(':nome', $pet);
            $query->bindParam(':raca', $raca);
            $query->bindParam(':sexo', $sexo);
            $query->bindParam(':porte', $porte);
            $query->bindParam(':data', $data);
            $query->bindParam(':obs', $obs);
            $query->bindParam(':fk_cliente', $dono);
            $query->execute();

            header('Location: http://localhost/pet-shop-workspace/pet-shop/view/Pet/pet_lista.php');
        } catch (Exception $e) {
            echo "Falha na inclusão:" . $e->getMessage();
        }
        ?>
    </body>
</html>
