<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<?php

global $uf;
global $cidade;

if (!empty($_GET["uf"])) {
    $uf = $_GET['uf'];
}

if (!empty($_GET["cidade"])) {
    $cidade = $_GET['cidade'];
}

include '../conectbd.php';

$query = $link->prepare("SELECT pk_cidade, nome FROM tb_cidade WHERE fk_uf = '" . $uf . "'");
$query->execute();

echo '<label style="margin: 15px;" for="cidades">Cidade</label>'
 . '<select style="width: 200px; margin-right: 15px;" id="cidade" name="cidades" class="form-control" required>';

while ($row = $query->fetch()) {
    if ($cidade == $row["pk_cidade"]) {
        echo "<option value='" . $row["pk_cidade"] . "' selected>" . $row["nome"] . "</option>";
    } else {
        echo "<option value='" . $row["pk_cidade"] . "'>" . $row["nome"] . "</option>";
    }
}
echo ' </select>';
?>
