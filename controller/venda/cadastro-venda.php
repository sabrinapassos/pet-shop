<?php

// efetua inclusao do curso informado em cadatro_curso.html

$valor = $_GET["total"];
$data = $_GET["data"];

if (!empty($_GET['idCliente'])) {
    $cliente = $_GET["idCliente"];
}

if (!empty($_GET['servId'])) {
    $servId = $_GET['servId'];
}

if (!empty($_GET["servPet"])) {
    $servPet = $_GET["servPet"];
}

if (!empty($_GET["servPreco"])) {
    $servPreco = $_GET["servPreco"];
}

if (!empty($_GET['produtoId'])) {
    $produtoId = $_GET["produtoId"];
}

if (!empty($_GET['qtdProd'])) {
    $qtdProd = $_GET['qtdProd'];
}





include '../../conectbd.php';
try {
    $query = $link->prepare("INSERT INTO tb_venda 
      (valor, fk_cliente, dt_venda) 
	  values (:valor, :fk_cliente, :dt_venda)");

    $query->bindParam(':valor', $valor);
    $query->bindParam(':fk_cliente', $cliente);
    $query->bindParam(':dt_venda', $data);
    $query->execute();
    $venda = $link->lastInsertId();
    
    if (!empty($produtoId) && count($produtoId) > 0) {
        for ($i = 0; $i < count($qtdProd); $i++) {
            $queryProd = $link->prepare("INSERT INTO tb_venda_produto (fk_venda, fk_produto, qtd_prod) 
                values (:fk_venda, :fk_produto, :qtd_prod)");

            $queryProd->bindParam(':fk_venda', $venda);
            $queryProd->bindParam(':fk_produto', $produtoId[$i]);
            $queryProd->bindParam(':qtd_prod', $qtdProd[$i]);
            $queryProd->execute();


            $query2 = $link->prepare("select qtd_prod from tb_produto where pk_produto = :pk_produto");
            $query2->bindParam(':pk_produto', $produtoId[$i]);
            $query2->execute();

            $row = $query2->fetch();

            $qtdProduto = $row["qtd_prod"];
            $qtdProduto = $qtdProduto - $qtdProd[$i];

            $queryProd = $link->prepare("update tb_produto set qtd_prod = :qtd where pk_produto = :pk_produto");
            $queryProd->bindParam(':pk_produto', $produtoId[$i]);
            $queryProd->bindParam(':qtd', $qtdProduto);
            $queryProd->execute();
        }
    }

    if (!empty($servId) && count($servId) > 0) {
        for ($i = 0; $i < count($servId); $i++) {
            $queryServ = $link->prepare("INSERT INTO tb_venda_servico (fk_venda, fk_servico, fk_pet) 
	  values (:fk_venda, :fk_servico, :fk_pet)");

            $queryServ->bindParam(':fk_venda', $venda);
            $queryServ->bindParam(':fk_servico', $servId[$i]);
            $queryServ->bindParam(':fk_pet', $servPet[$i]);
            $queryServ->execute();
            $venda_serv = $link->lastInsertId();

            $queryOS = $link->prepare("INSERT INTO tb_ordem_servico (fk_venda, fk_venda_servico) 
	  values (:fk_venda, :fk_venda_servico)");

            $queryOS->bindParam(':fk_venda', $venda);
            $queryOS->bindParam(':fk_venda_servico', $venda_serv);
            $queryOS->execute();
        }
    }

   header('Location: http://localhost/pet-shop-workspace/pet-shop/view/venda/venda_lista.php');
   
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}
?>




