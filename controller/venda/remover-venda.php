<?php

session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>

<?php

$id = $_GET["id"];

include '../../conectbd.php';

try {


    $query = $link->prepare("delete from tb_venda_produto  where fk_venda=:id");
    $query->bindParam(':id', $id);
    $query->execute();

    $query = $link->prepare("delete from tb_ordem_servico  where fk_venda=:id");
    $query->bindParam(':id', $id);
    $query->execute();

    $query = $link->prepare("delete from tb_venda_servico where fk_venda=:id");
    $query->bindParam(':id', $id);
    $query->execute();

    $query = $link->prepare("delete from tb_venda  where pk_venda=:id");
    $query->bindParam(':id', $id);
    $query->execute();

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/venda/venda_lista.php');
    
} catch (PDOException $e) {
    echo "Falha na exclusão: " . $e->getMessage();
}
?>  
