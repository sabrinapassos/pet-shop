<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<?php

$id = $_GET["id"];

include '../../conectbd.php';

try {

    $queryend = $link->prepare("select fk_endereco from tb_fornecedor  where pk_fornecedor=:id");
    $queryend->bindParam(':id', $id);
    $queryend->execute();

    $row = $queryend->fetch();
    if (!empty($row["fk_endereco"])) {
        $endereco = $row["pk_fornecedor"];
        $queryend = $link->prepare("delete from tb_endereco where pk_endereco=:id");
        $queryend->bindParam(':id', $endereco);
        $queryend->execute();
    }


    $queryTel = $link->prepare("delete from tb_telefone_fornecedor  where fk_fornecedor=:id");
    $queryTel->bindParam(':id', $id);
    $queryTel->execute();

    $query = $link->prepare("delete from tb_fornecedor where pk_fornecedor=:id");

    $query->bindParam(':id', $id);
    $query->execute();

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/fornecedor/fornecedor-lista.php');
} catch (PDOException $e) {
    echo "Falha na exclusão: " . $e->getMessage();
}
?>  
