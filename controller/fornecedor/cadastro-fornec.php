<?php

if (!empty($_GET["id"])) {
    $id = $_GET["id"];
}

$nmFantasia = $_GET["nmFantasia"];
$rzSocial = $_GET["rzSocial"];
$cnpj = $_GET["cnpj"];
$cnpj = str_replace("-", "", $cnpj);
$cnpj = str_replace(".", "", $cnpj);
$cnpj = str_replace("/", "", $cnpj);
$logradouro = $_GET["log"];
$complemento = $_GET["complemento"];
$bairro = $_GET["bairro"];
$cep = $_GET["cep"];
$cep = str_replace("-", "", $cep);
$cidade = $_GET["cidades"];

if (!empty($_GET['tipo'])) {
    $selectTipo = $_GET['tipo'];
}
if (!empty($_GET["ddd"])) {
    $ddd = $_GET["ddd"];
}
if (!empty($_GET["numTelefone"])) {
    $numTel = $_GET["numTelefone"];
}

include '../../conectbd.php';
try {
    if (!empty($id)) {

        $queryFornecedor = $link->prepare("UPDATE tb_fornecedor   
            SET razao_social = :razao_social, nome_fantasia = :nome_fantasia, 
            cnpj = :cnpj  WHERE pk_fornecedor = :pk_fornecedor");

        $queryFornecedor->bindParam(':razao_social', $rzSocial);
        $queryFornecedor->bindParam(':nome_fantasia', $nmFantasia);
        $queryFornecedor->bindParam(':cnpj', $cnpj);
        $queryFornecedor->bindParam(':pk_fornecedor', $id);
        $queryFornecedor->execute();

        $queryend = $link->prepare("select fk_endereco from tb_fornecedor  where pk_fornecedor=:id");
        $queryend->bindParam(':id', $id);
        $queryend->execute();

        $row = $queryend->fetch();
        if (!empty($row["fk_endereco"])) {
            $endereco = $row["fk_endereco"];
            $queryEndereco = $link->prepare("UPDATE tb_endereco SET logradouro = :logradouro, complemento = :complemento,
                bairro = :bairro, fk_cidade = :fk_cidade, cep = :cep WHERE pk_endereco=:id");
            $queryEndereco->bindParam(':logradouro', $logradouro);
            $queryEndereco->bindParam(':complemento', $complemento);
            $queryEndereco->bindParam(':bairro', $bairro);
            $queryEndereco->bindParam(':fk_cidade', $cidade);
            $queryEndereco->bindParam(':cep', $cep);
            $queryEndereco->bindParam(':id', $endereco);
            $queryEndereco->execute();
        }


        if (!empty($numTel) && count($numTel) > 0) {

            $queryTel = $link->prepare("delete from tb_telefone_fornecedor  where fk_fornecedor=:id");
            $queryTel->bindParam(':id', $id);
            $queryTel->execute();

            for ($i = 0; $i < count($numTel); $i++) {

                $queryTelefone = $link->prepare("INSERT INTO tb_telefone_fornecedor    
            (fk_fornecedor, ddd, telefone, fk_tipo) 
	  values (:fk_fornecedor, :ddd, :telefone, :fk_tipo)");

                $queryTelefone->bindParam(':fk_fornecedor', $id);
                $queryTelefone->bindParam(':ddd', $ddd[$i]);
                $queryTelefone->bindParam(':telefone', $numTel[$i]);
                $queryTelefone->bindParam(':fk_tipo', $selectTipo[$i]);
                $queryTelefone->execute();
            }
        }
    } else {
        $queryEndereco = $link->prepare("INSERT INTO tb_endereco  
            (logradouro, complemento, bairro, fk_cidade, cep) 
	  values (:logradouro, :complemento, :bairro, :fk_cidade, :cep)");

        $queryEndereco->bindParam(':logradouro', $logradouro);
        $queryEndereco->bindParam(':complemento', $bairro);
        $queryEndereco->bindParam(':bairro', $complemento);
        $queryEndereco->bindParam(':fk_cidade', $cidade);
        $queryEndereco->bindParam(':cep', $cep);
        $queryEndereco->execute();


        $endereco = $link->lastInsertId();
        $queryFornecedor = $link->prepare("INSERT INTO tb_fornecedor   
            (razao_social, nome_fantasia, cnpj, fk_endereco ) 
	  values (:razao_social, :nome_fantasia, :cnpj, :fk_endereco)");

        $queryFornecedor->bindParam(':razao_social', $rzSocial);
        $queryFornecedor->bindParam(':nome_fantasia', $nmFantasia);
        $queryFornecedor->bindParam(':cnpj', $cnpj);
        $queryFornecedor->bindParam(':fk_endereco', $endereco);
        $queryFornecedor->execute();

        $fornecedor = $link->lastInsertId();

        if (!empty($numTel) && count($numTel) > 0) {
            for ($i = 0; $i < count($numTel); $i++) {

                $queryTelefone = $link->prepare("INSERT INTO tb_telefone_fornecedor    
            (fk_fornecedor, ddd, telefone, fk_tipo) 
	  values (:fk_fornecedor, :ddd, :telefone, :fk_tipo)");

                $queryTelefone->bindParam(':fk_fornecedor', $fornecedor);
                $queryTelefone->bindParam(':ddd', $ddd[$i]);
                $queryTelefone->bindParam(':telefone', $numTel[$i]);
                $queryTelefone->bindParam(':fk_tipo', $selectTipo[$i]);
                $queryTelefone->execute();
            }
        }
    }


    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/fornecedor/fornecedor-lista.php');
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}
?>  




