<?php
session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>

<!DOCTYPE html>
<!-- inclusao.php -->
<html>
    <head>
        <title>Cadastro de OS - Inclusão</title>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
        $venda = $GET["fk_venda"];
        $data = $_GET["data"];
        $duracao = $_GET["duracao"];
        $horario = $_GET["horario"];
        $obs = $_GET["observacoes"];

        include '../../conectbd.php';
        try {
            $query = $link->prepare("INSERT INTO tb_ordem_servico 
      (data, duracao, horario, observacoes, fk_venda) 
	  values (:data, :duracao, :horario, :observacoes, :fk_venda)");

            $query->bindParam(':data', $data);
            $query->bindParam(':duracao', $duracao);
            $query->bindParam(':horario', $horario);
            $query->bindParam(':observacoes', $obs);
            $query->bindParam(':fk_venda', $venda);
            $query->execute();

            echo "<h1> Cadastro Realizado com sucesso. </h1>";

            echo"<script language='javascript' type='text/javascript'> window.open('../../view/ordem_servico_lista.php', '_self');</script>";

            // header('Location: ../../view/ordem_servico_lista.php');
        } catch (PDOException $e) {
            echo "Falha na inclusão: " . $e->getMessage();
        }
        ?>  


    </body>
</html>

