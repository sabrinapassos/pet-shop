<?php

session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>
<?php

$id = $_POST["id"];
$email = $_POST["nome"];
$senha = md5($_POST["senha"]);
$cpf = $_POST["cpf"];
$cpf = str_replace(".", "", $cpf);
$cpf = str_replace("-", "", $cpf);
$nome = $_POST["nome"];
$dtnasc = $_POST["dtnasc"];

include '../../conectbd.php';
try {
    $query = $link->prepare("UPDATE tb_usuario 
        SET email = :email,
            senha = :senha,
            nome = :nome,
            cpf = :cpf,
            dt_nasc = :dt_nasc
        WHERE pk_usuario = :id ");

    $query->bindParam(':email', $email);
    $query->bindParam(':senha', $senha);
    $query->bindParam(':nome', $nome);
    $query->bindParam(':cpf', $cpf);
    $query->bindParam(':dt_nasc', $dtnasc);
    $query->bindParam(':id', $id);
    $query->execute();

    echo "Alterado com precisão.";
} catch (PDOException $e) {
    echo "Falha na inclusão: " . $e->getMessage();
}

    header('Location: http://localhost/pet-shop-workspace/pet-shop/view/usuario/usuario_lista.php');

?>

