<?php
session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>

<!DOCTYPE html>
<!-- cadastro.html -->
<html>
    <head>
        <title>Usuario - Exclusão</title>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
        $id = $_GET["id"];

        include '../../conectbd.php';

        try {

            $query = $link->prepare("delete from tb_usuario  where pk_usuario =:id");
            $query->bindParam(':id', $id);
            $query->execute();

            header('Location: http://localhost/pet-shop-workspace/pet-shop/view/usuario/usuario_lista.php');
        } catch (PDOException $e) {
            echo "Falha na exclusão: " . $e->getMessage();
        }
        ?>  
        <br>

    </body>
</html>

