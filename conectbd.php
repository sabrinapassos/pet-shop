<?php

$servername = "localhost";
$username = "root";
$password = "****";
$dbname = "pet_shop";

try {
    $link = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Conexão estabelecida";
} catch (PDOException $e) {
    echo "Falha na conexão: " . $e->getMessage();
}

?>