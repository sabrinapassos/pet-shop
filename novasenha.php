<!DOCTYPE html>
<?php 
global $id;
$id = $_GET["id"];
?>
<html>
    <head>
        <script src="assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/jquery.mask.js" type="text/javascript"></script>
        <link href="assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Recuperar Senha</title>
        <script>
            function validarSenha(){
               novasenha = document.getElementById('novasenha').value;
               cnovasenha = document.getElementById('cnovasenha').value;
               if (novasenha != cnovasenha) {
                  alert("Senhas diferentes!\nFavor digitar senhas iguais"); 
               }else{
                  document.rec.submit();
               }
            }
        </script>
    </head> 
    <body style="background-image: url('assets/images/dog.jpg');">
<!--    style="background-image: url('assets/images/dog.jpg');"-->
        
        <div class="container"> 
            <div class="row" style="padding-top: 200px;">
                <div class="col-md-4">&nbsp;</div>
                <form id="rec" name="rec" class="form-horizontal col-md-4" action="novanova.php" 
                      method="POST" style="background-color: white; opacity: 0.9; margin: 20px; padding: 20px;">
                    <fieldset>
                        <legend><center>Definir nova senha</center></legend>
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>

                     
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="novasenha">Nova senha</label>  
                            <div class="col-md-9">
                                <input id="novasenha" name="novasenha" type="password" placeholder="Insira sua nova senha" class="form-control form-control-lg cpf">
                            </div>
                        </div>
                        
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="cnovasenha">Confirmar Senha</label>  
                            <div class="col-md-9">
                                <input id="cnovasenha" name="cnovasenha" type="password" placeholder="Insira novamente a senha" class="form-control form-control-lg cpf">
                            </div>
                        </div>
                        
                        <!-- Button -->

                        <button id="singlebutton" name="send" type="button" class="btn btn-success col-md-12" onClick="validarSenha()">Trocar senha</button>

                    </fieldset>
                </form>    
                <div class="col-md-4">&nbsp;</div>
            </div>

        </div>    
    </body>
</html>
