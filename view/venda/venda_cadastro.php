<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de Vendas</title>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    </head>
    
    <body style="font-size: 12px">

        <?php
        include '../../conectbd.php';
        ?>

        <?php
        global $cpf;

        if (!empty($_GET["cpf"])) {
            $cpf = $_GET['cpf'];

            $cpf = str_replace("-", "", $cpf);
            $cpf = str_replace(".", "", $cpf);
            $cpf = str_replace("/", "", $cpf);
        }


        include '../../conectbd.php';

        $query = $link->prepare("SELECT pk_cliente, nome_cliente FROM tb_cliente WHERE cpf = " . $cpf . "");
        $query->execute();
        $row = $query->fetch();

        global $idCliente;
        $idCliente = $row["pk_cliente"];

        if ($idCliente != null && $idCliente != 'undefined' && $idCliente != '') {
            echo "<h3 style='text-align: center; padding-bottom: 20px;'>Cliente: $row[nome_cliente]</h3>";
            echo "<input  name='idCliente' type='hidden' value='$row[pk_cliente]'/>";
        } else {
            $display = "display: none;";
            echo "<h4 style='text-align: center; padding-bottom: 20px;'>Cliente não Cadastrado. Faça o cadastro para realizar a venda de um serviço!</h4>";
            echo "<script> $('#servicosAdd').attr('style', 'display: none'); </script>";
            
        }
        ?>



        <div class="container">
            <div class="row">
                <form class="form-horizontal" action="../../controller/venda/cadastro-venda.php">
                    <div style="padding-top: 30px">
                        <input  name='idCliente' type="hidden"  value="<?php echo "$idCliente"; ?>"/>
                         <div class="form-group">
                        <label class="col-md-4 control-label" for="datanasc">Data da venda</label>  
                        <div class="col-md-4">
                            <input id="textinput" name="data" type="date" class="form-control input-md" required>
                        </div>
                         </div>
                        <fieldset>
                            <legend style="text-align: center">Produtos</legend>
                            <div class="form-group form-inline">
                                <label style="padding: 10px;" for="prod">Produto </label>  
                                <select style="width:400px;"  id="prod" name="prod" class="form-control" >
                                    <option value="">Selecione...</option>
                                    <?php
                                    $queryProd = $link->prepare("SELECT pk_produto, descricao_prod, qtd_prod, preco_prod FROM tb_produto WHERE qtd_prod > 0;");
                                    $queryProd->execute();
                                     
                                    $row = $queryProd->setFetchMode(PDO::FETCH_ASSOC);
                                    while ($row = $queryProd->fetch()) {
                                        $id = $row["pk_produto"];
                                        $descricao = $row["descricao_prod"];
                                        $preco = $row["preco_prod"];
                                        $qtd = $row["qtd_prod"];

                                        echo "<option data-preco='$preco' data-qtd='$qtd' value='$id'>$descricao</option>";
                                    }
                                    ?>
                                </select>
                                <label style="padding: 10px;" class="control-label" for="qtd"> Quantidade </label>  
                                <input style="width:50px;" id="qtd" name="qtd" type="number" class="form-control" value="1"/>
                                <a onclick="adicionarProduto();" class="btn btn-primary btnCenter">+ Adicionar</a>
                            </div>

                            <table id="prodTable" class="table table-over" width="100%">
                                <thead >
                                    <tr>  
                                        <th>Código</th>
                                        <th>Produto</th>
                                        <th>Quantidade</th>
                                        <th>Preço</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>


                                    </tr>
                                </tbody>    
                            </table>
                        </fieldset>
                    </div>
                    <div id="servicosAdd">
                        <fieldset>
                            <legend style="text-align: center">Serviços</legend>
                            <div class="form-group form-inline">
                                <label style="padding: 10px;" for="serv">Serviço </label>  
                                <select style="width:400px;"  id="serv" name="serv" class="form-control" >
                                   
                                    <?php
                                    $queryServ = $link->prepare("SELECT pk_servico, servico , preco FROM tb_servico;");
                                    $queryServ->execute();

                                    $row = $queryServ->setFetchMode(PDO::FETCH_ASSOC);
                                    while ($row = $queryServ->fetch()) {
                                        $id = $row["pk_servico"];
                                        $descricao = $row["servico"];
                                        $preco = floatval($row["preco"]);

                                        echo "<option data-preco='$preco' value='$id'>$descricao</option>";
                                    }
                                    ?>
                                </select>

                                <label style="padding: 10px;" for="pet">Pet </label>  
                                <select style="width:300px;"  id="pet" name="pet" class="form-control" >
                                    
                                    <?php
                                    $queryServ = $link->prepare("SELECT pk_pet, nome_pet  FROM tb_pet WHERE fk_cliente = '" . $idCliente . "'");
                                    $queryServ->execute();

                                    $row = $queryServ->setFetchMode(PDO::FETCH_ASSOC);
                                    while ($row = $queryServ->fetch()) {
                                        $id = $row["pk_pet"];
                                        $nome_pet = $row["nome_pet"];

                                        echo "<option value='$id'>$nome_pet</option>";
                                    }
                                    ?>
                                </select>

                                <a onclick="adicionarServico();" class="btn btn-primary btnCenter">+ Adicionar</a>
                            </div>
                            <table id="servList" class="table table-hover" width="100%">
                                <thead>
                                    <tr>    
                                        <th>Código</th>
                                        <th>Serviço</th>
                                        <th>Pet</th>
                                        <th>Preço</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>


                                    </tr>
                                </tbody>    
                            </table>
                        </fieldset>
                    </div>
                    <div class="form-group form-inline" style="padding-bottom: 20px">
                        <label class="col-md-4 control-label" for="total">Total</label>  
                        <div class="col-md-5 ">
                            <input style="width: 100px;" id="total" name="total" class="form-control input-md" size="50" readonly />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5">&nbsp;</div>
                        <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Salvar"/>

                        <input id="cancelar" name="cancelar" type="button" onclick="window.open('venda_lista.php');" class="btn btn-primary btnCenter" value="Cancelar"/>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>



