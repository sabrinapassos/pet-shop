<?php
session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de Vendas</title>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    </head>
    <script>
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });

        function showCliente(cpf) {
            $("#vendaCadastro").load("venda_cadastro.php?cpf=" + cpf);
        }

        function adicionarProduto() {

            var produtoId = $('#prod').val();
            if (produtoId != null && produtoId != "" && produtoId != 'undefined') {
                var produtoTexto = $('#prod>option:selected').text();

                var produtoPreco = parseFloat($('#prod>option:selected').attr("data-preco"));
                var qtdProduto = parseFloat($('#prod>option:selected').attr("data-qtd"));
                var quantidade = parseFloat($('#qtd').val());

                if (quantidade > qtdProduto) {
                    alert("Quantidade indisponivel!");

                } else {
                    if (quantidade > 1) {
                        produtoPreco += parseFloat(produtoPreco * quantidade);
                    }


                    var newRow = $('<tr id="trProd' + produtoId + '">');
                    var cols = "";
                    cols += '<td>' + produtoId + '<input type="hidden" name="produtoId[]" value="' + produtoId + '"/></td>';
                    cols += '<td>' + quantidade + '<input type="hidden" name="qtdProd[]" value="' + quantidade + '"/></td>';
                    cols += '<td>' + produtoTexto + '<input type="hidden" name="produtoTexto[]" value="' + produtoTexto + '"/></td>';
                    cols += '<td>' + produtoPreco + '<input class="valor" type="hidden" name="produtoPreco[]" value="' + parseFloat(produtoPreco) + '"/></td>';
                    cols += '<td>';
                    cols += '<a onclick="removeTableRow(\'trProd' + produtoId + '\')" type="button">Remover</a>';
                    cols += '</td>';
                    newRow.append(cols);
                    $("#prodTable").append(newRow);

                    recalcularTotal();
                }

            }
        }

        function adicionarServico() {

            var servId = $('#serv').val();
            if (servId != null && servId != "" && servId != 'undefined') {


                var servTexto = $('#serv>option:selected').text();
                var servPreco = $('#serv>option:selected').attr("data-preco");
                var quantidade = $('#qtdServico').text();
                var servPet = $('#pet').text();
                var servPetId = $('#pet').val();

                if (quantidade > 1) {
                    servPreco += parseFloat(servPreco * quantidade);
                }


                var newRow = $('<tr id="trServ' + servId + '">');
                var cols = "";
                cols += '<td>' + servId + '<input type="hidden"  name="servId[]" value="' + servId + '"/></td>';
                cols += '<td>' + servTexto + '<input type="hidden" name="servTexto[]" value="' + servTexto + '"/></td>';
                cols += '<td>' + servPet + '<input type="hidden" name="servPet[]" value="' + servPetId + '"/></td>';
                cols += '<td>' + servPreco + '<input class="valor" type="hidden" name="servPreco[]" value="' + servPreco + '"/></td>';
                cols += '<td>';
                cols += '<a onclick="removeTableRow(\'trServ' + servId + '\');" type="button">Remover</a>';
                cols += '</td>';
                newRow.append(cols);
                $("#servList").append(newRow);

                recalcularTotal();
            }
        }

        function recalcularTotal() {
            var sum = 0;
            $('.valor').each(function () {
                sum += parseFloat(this.value);
            });

            $("#total").val("" + sum + "");
        }

        function removeTableRow(linha) {
            $("#" + linha).remove();
            recalcularTotal();
        }

    </script>
    <body style="font-size: 12px">

        <?php
        include '../menu.php';
        include '../../conectbd.php';
        ?>



        <div class="container">
            <div class="row">
                <form class="form-horizontal">
                    <!-- Form Name -->
                    <h2 style="text-align: center">Cadastro de Vendas</h2>

                    <div style="padding-top: 30px">

                        <!-- Text input-->
                        <h3 style="text-align: center; padding-bottom: 20px;">Cliente</h3>
                        <div class="form-group form-inline" style="padding-bottom: 20px">
                            <label class="col-md-4 control-label" for="cpf">CPF</label>  
                            <div class="col-md-5 ">
                                <input id="cpfinput" name="cpf" type="text" class="form-control input-md cpf" size="50" />
                                <a onclick="showCliente($('#cpfinput').val())"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                            </div>
                        </div>
                </form>

                <div id="vendaCadastro">

                </div>
            </div>
        </div>
    </body>
</html>

