<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Lista de Vendas</title>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>       
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/DataTables-1.10.16/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <script>
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });



        $(document).ready(function () {

            $('#list').DataTable({
                language: {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
        });

        function showModal(str) {
            $("#modalVenda").load("../../controller/venda/venda-visualizar.php?id=" + str);
        }

    </script>

    <body style="font-size: 12px">

        <?php include '../menu.php' ?>

        <?php
        include_once '../../conectbd.php';

        $query = $link->prepare("select v.pk_venda, v.valor, v.fk_cliente, DATE_FORMAT(v.dt_venda, '%d/%m/%Y') dt_venda, c.cpf, c.nome_cliente from tb_venda v
            left join tb_cliente c on c.pk_cliente = v.fk_cliente order by v.dt_venda desc;");
        $query->execute();
        $row = $query->setFetchMode(PDO::FETCH_ASSOC);
        ?>

        <div class="container">
            <h2 style="text-align: center">Lista de Vendas</h2>
            <div style="padding: 10px;">
                <a href="../venda/consulta-cliente.php" class="btn btn-primary btnCenter">+ Adicionar</a>
            </div>
            <table id="list" class="table table-hover" width="100%">
                <thead>
                    <tr>    
                        <th>Código</th>
                        <th>Data da Venda</th>
                        <th>Cliente</th>
                        <th>CPF</th>
                        <th>Valor</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    function mask($val, $mask) {
                        $maskared = '';
                        $k = 0;
                        
                        for ($i = 0; $i <= strlen($mask) - 1; $i++){
                            if ($mask[$i] == '#'){
                                if (isset($val[$k]))
                                $maskared .= $val[$k++];
                            } else {
                                if (isset($mask[$i]))
                                $maskared .= $mask[$i];
                            }
                        }
                        return $maskared;
                    }



                    while ($row = $query->fetch()) {
                        $id = $row["pk_venda"];
                        $nome = $row["nome_cliente"];
                        $cpf = $row["cpf"];
                        $valor = $row["valor"];
                        $data = $row["dt_venda"];
                        echo "<tr>";
                        echo "<td>" . $id . "</td>";
                        echo "<td>" . $data . "</td>";
                        echo "<td>" . $nome . "</td>";
                        echo "<td>" . $cpf . "</td>";
                        echo "<td>" . $valor . "</td>";
                        echo '<td><a onclick="showModal(' . $row["pk_venda"] . ')" data-toggle="modal" data-target="#myModal">Ver mais...</a> | <a onclick="return confirm(\'Deseja excluir esse registro?\');" href="../../controller/venda/remover-venda.php?id=' . $row["pk_venda"] . '">Remover</a></td>';

                        echo "</tr>";
                    }
                    ?>
                </tbody>   
            </table>
        </div>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Venda</h4>
                    </div>
                    <div class="modal-body" id="modalVenda">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
