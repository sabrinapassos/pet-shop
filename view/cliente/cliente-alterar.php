<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Alteração de Cliente</title>

        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <script>

        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('000000000');
            $('.ddd').mask('00');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });




        var indexTel = 1;

        function removerTelefone(id) {
            $("#" + id).remove();
        }

        function adicionarTelefone() {
            $("#telNone").children()[0].id = "telefone" + indexTel++;
            var telDiv = $("#telNone").html();
            $("#telefone").append(telDiv);
        }

        function showCidades(idUf, cidade) {
            $("#selectCidade").load("../../controller/cidades.php?uf=" + idUf + "&cidade=" + cidade);
        }
        function validarcpf(str) {
            $("#validarcpf").load("../../controller/validar_cpf.php?cpf=" + str);
        }

    </script>

    <body style="font-size: 12px">

        <?php
        include '../menu.php';
        include '../../conectbd.php';

        //cliente
        global $id;
        global $row;
        global $idcliente;
        global $nome;
        global $cpf;
        global $rg;
        global $email;
        global $dtnasc;
        //endereço
        global $log;
        global $bairro;
        global $complemento;
        global $uf;
        global $cidade;
        global $cep;

        if (!empty($_GET["id"])) {
            $id = $_GET["id"];


            $query = $link->prepare
                    (" SELECT  c.pk_cliente,c.nome_cliente ,c.cpf,c.rg,c.email,c.datanasc,
                    e.pk_endereco,e.logradouro,e.bairro,e.cep,e.complemento,u.estado,
                    ci.pk_cidade,ci.nome ,u.pk_uf, ci.pk_cidade
            FROM tb_cliente c
            LEFT JOIN tb_endereco e ON c.fk_endereco = e.pk_endereco
            LEFT JOIN tb_cidade ci ON e.fk_cidade=ci.pk_cidade
            LEFT JOIN tb_uf u ON ci.fk_uf=u.pk_uf
            WHERE c.pk_cliente = '" . $id . "';");
            $query->execute();
            $row = $query->Fetch();

            //cliente        
            $idcliente = $row["pk_cliente"];
            $nome = $row["nome_cliente"];
            $cpf = $row["cpf"];
            $rg = $row["rg"];
            $email = $row["email"];
            $dtnasc = $row["datanasc"];
            //endereço
            $log = $row["logradouro"];
            $bairro = $row["bairro"];
            $complemento = $row["complemento"];
            $uf = $row["estado"];
            $cidade = $row["pk_cidade"];
            $cep = $row["cep"];

            $queryTel = $link->prepare("SELECT tb_telefone_cliente.pk_telefone_cliente,tb_tipo_telefone.tipo,
                                      tb_telefone_cliente.ddd,tb_telefone_cliente.telefone
                               fROM tb_telefone_cliente
                               INNER JOIN tb_tipo_telefone 
                               ON tb_telefone_cliente.fk_tipo=tb_tipo_telefone.pk_tipo_telefone
                               Where fk_cliente ='" . $id . "'");
        }
        ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/Cliente/alteração-cliente.php">
                    <input type="hidden" name="id" value="<?php echo"$id" ?>"/>
                    <!-- Form Name -->
                    <h2 style="text-align: center">Cadastro de clientes</h2>

                    <!-- Nome -->

                    <div class="form-group form-inline">
                        <center>
                            <label for="nomecliente">Nome </label>
                            <input style="width: 685px; margin-left: 10px;" id="textinput" name="nomecliente" type="text" class="form-control input-md"  value="<?php echo"$nome" ?>"  required>
                        </center>
                    </div>


                    <!-- CPF -->
                    <div class="form-group form-inline">
                        <center>

                            <!-- RG -->
                            <label  for="rg">RG</label> 
                            <input id="rg" name="rg" type="text"  class="form-control input-md" value="<?php echo"$rg" ?>"
                                   style="width: 150px; margin-left: 10px;" required onkeyup="maskIt(this, event, '###.###.###', true)">
                            <div class="col-md-12" id="validarcpf"></div>
                            <label style="margin-left: 10px;" for="cpf">CPF</label>  
                            <input id="cpf" name="cpf" type="text" class="form-control input-md" value="<?php echo"$cpf" ?>"  
                                   style="width: 180px; margin-left: 10px;" required onkeyup="maskIt(this, event, '###.###.###-##', true)" 
                                   onchange="validarcpf(this.value)">
                            <label  style="margin-left: 10px;" for="datanasc">Data de nascimento</label> 
                            <input id="textinput" name="datanasc" type="date" class="form-control input-md" value="<?php echo"$dtnasc" ?>" style="width: 160px;" required>
                        </center>
                    </div>

                    <!-- Email -->
                    <div class="form-group form-inline">
                        <center>
                            <label for="email">E-mail</label>  
                            <input style="width: 685px; margin-left: 10px;" id="textinput" name="email" type="text" value="<?php echo"$email" ?>" class="form-control input-md" style="width: 300px;" required>
                        </center>
                    </div>




                    <!-- telefone -->

                    <div style="padding-top: 30px">
                        <fieldset>
                            <legend style="text-align: center">Telefone</legend>

                            <!-- Select Basic -->

                            <a href="#" id="add" name="add" class="btn btn-primary" onclick="adicionarTelefone();"
                               style="text-align: center; margin-left: 25%;">+ Adicionar</a>
                            <div  id="telefone">
                                <?php
                                if (!empty($id)) {
                                    $queryTel = $link->prepare("select tp.tipo, f.ddd, f.telefone from tb_telefone_funcionario  f inner join tb_tipo_telefone tp on  f.fk_tipo = tp.pk_tipo_telefone where fk_funcionario = '" . $id . "'");
                                    $queryTel->execute();

                                    echo "<div class='form-group form-inline' style='margin: 10px;'  >";
                                    while ($rowTel = $queryTel->fetch()) {
                                        echo " <label class='col-md-4 control-label' for='telfuncionario'>Tipo</label>"
                                        . " <div class='col-md-8' style='text-align: left; padding-bottom: 10px;'>"
                                        . " <select style=' margin-right: 15px;'  id='selectTipo' name='tipo[]' class='form-control' required>";

                                        $queryTipo = $link->prepare("SELECT pk_tipo_telefone, tipo FROM tb_tipo_telefone;  ");
                                        $queryTipo->execute();

                                        $row = $queryTipo->setFetchMode(PDO::FETCH_ASSOC);
                                        while ($row = $queryTipo->fetch()) {
                                            $id = $row["pk_tipo_telefone"];
                                            $tipo = $row["tipo"];
                                            if ($tipo == $rowTel['tipo']) {
                                                echo "<option value = '$id' selected>$tipo</option>";
                                            } else {
                                                echo "<option value = '$id'>$tipo</option>";
                                            }
                                        }

                                        echo ' </select> 
                                                    <label class="control-label" for="ddd">DDD</label> 
                                                    <input style=" margin-right: 15px; "  id="selectbasic" name="ddd[]" maxlength="2" size="2" 
                                                    class="form-control ddd" value="' . $rowTel["ddd"] . '">
                                                    <label class="control-label" for="numTelefone">Telefone</label>  
                                                    <input  style=" margin-right: 15px;"  id="numTel" name="numTelefone[]" type="text" 
                                                    class="form-control phone" value="' . $rowTel["telefone"] . '" required>
                                                    <a href="#" onclick="removerTelefone($(this).parent().parent().attr(\'id\'));"><b>X</b></a></div>';
                                    }
                                    echo "</div>";
                                }
                                ?>


                            </div>
                        </fieldset>
                    </div>
                    <div style="padding-top: 30px">
                        <fieldset>
                            <legend style="text-align: center">Endereço</legend>

                            <div class="form-group form-inline">
                                <center>
                                    <label  for="log">Logradouro</label>  
                                    <input style="width: 400px; margin: 5px;" id="log" name="log" type="text" class="form-control input-md"  value="<?php echo"$log" ?>"  required/>
                                    <label for="bairro">Bairro</label>
                                    <input style="width: 400px; margin: 5px;" id="bairro" name="bairro" type="text" class="form-control input-md" value="<?php echo"$bairro" ?>" required/>
                                </center>
                            </div>

                            <div class="form-group form-inline">
                                <center>
                                    <label  for="complemento">Complemento</label>  
                                    <input style="width: 850px; margin: 5px;" id="complemento" name="complemento" type="text"   value="<?php echo"$complemento" ?>" 
                                           class="form-control input-md" required/>
                                </center>
                            </div>
                            <div class="form-group form-inline">
                                <center>
                                    <label for="cep">CEP</label> 
                                    <input id="cep" name="cep" type="text" class="form-control input-md cep" value="<?php echo"$cep" ?>" required/>
                                    <label  for="uf">UF</label>
                                    <select id="uf" name="uf" class="form-control" required style="width: 110px;" onchange="showCidades(this.value)" required>
                                        <option value="">Selecione...</option>
                                        <?php
                                        $queryUf = $link->prepare("SELECT pk_uf, estado FROM tb_uf;");
                                        $queryUf->execute();

                                        $row = $queryUf->setFetchMode(PDO::FETCH_ASSOC);
                                        while ($row = $queryUf->fetch()) {
                                            $id = $row["pk_uf"];
                                            $estado = $row["estado"];

                                            if ($uf == $estado) {
                                                echo "<option value='$id' selected>$id</option>";
                                                $idUf = $id;
                                            } else {
                                                echo "<option value='$id'>$id</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <div style=" margin-right: 15px;"  id="selectCidade" onclick="window.open('cliente_lista.php');" name="selectCidade" class="form-group form-inline"> </div>
                                </center>
                            </div>
                        </fieldset>  
                    </div>

                    <div class="form-group">
                        <div class="col-md-5">&nbsp;</div>
                        <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Salvar"/>

                        <a href="cliente_lista.php"> <input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/></a>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div style="display: none;" id="telNone">
        <div class="form-group form-inline" style="margin: 10px;"  >
            <label class="col-md-4 control-label" for="telfuncionario">Tipo</label> 
            <div class="col-md-8" style="text-align: left;">
                <select style=" margin-right: 15px;"  id="selectTipo" name="tipo[]" class="form-control" required>
                    <?php
                    $queryTipo = $link->prepare("SELECT pk_tipo_telefone, tipo FROM tb_tipo_telefone ;");
                    $queryTipo->execute();

                    $row = $queryTipo->setFetchMode(PDO::FETCH_ASSOC);
                    while ($row = $queryTipo->fetch()) {
                        $id = $row["pk_tipo_telefone"];
                        $tipo = $row["tipo"];
                        echo "<option value='$id'>$tipo</option>";
                    }
                    ?>
                </select>
                <label class="control-label" for="ddd">DDD</label>  
                <input style=" margin-right: 15px;"  id="selectbasic" name="ddd[]" maxlength="2" size="2" class="form-control ddd">
                <label class="control-label" for="numTelefone">Telefone</label>  
                <input  style=" margin-right: 15px;"  id="numTel" name="numTelefone[]" type="text" class="form-control phone" required>
                <a href="#" onclick="removerTelefone($(this).parent().parent().attr('id'));"><b>X</b></a>
            </div> 
        </div>
    </div>

</body>
</html>

<?php
if ($idUf != null) {
    echo " <script> showCidades('$idUf', '$cidade'); </script>";
}
?>





