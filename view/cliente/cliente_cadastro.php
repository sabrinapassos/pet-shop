<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Cadastro de Clientes</title>

        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script type="text/javascript">
            $(document).ready(function () {
                $('#cpf').mask('000.000.000-00', {reverse: true});
                $('#rg').mask('0.000.000', {reverse: true});
                $('#cep').mask('00000-000');
                $('.phone').mask('000000000');
                $('.ddd').mask('00');
            })
        </script>

    </head>

    <script>
        var indexTel = 1;

        function removerTelefone(id) {
            $("#" + id).remove();
        }

        function adicionarTelefone() {
            $("#telNone").children()[0].id = "telefone" + indexTel++;
            var telDiv = $("#telNone").html();
            $("#telefone").append(telDiv);
        }

        function showCidades(str) {
            $("#selectCidade").load("../../controller/cidades.php?uf=" + str);
        }

        function validarcpf(str) {
            $("#validarcpf").load("../../controller/validar_cpf.php?cpf=" + str);
        }
    </script>

    <body style="font-size: 12px">

        <?php
        include '../menu.php';
        include '../../conectbd.php';
        ?>

        <div class="container">

            <form class="form-horizontal" action="../../controller/Cliente/cadastro-cliente.php">
                <div class="row">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Cadastro de clientes</h2>

                    <!-- Nome -->

                    <div class="form-group form-inline">
                        <center>
                            <label for="nomecliente">Nome </label>
                            <input style="width: 685px; margin-left: 10px;" id="textinput" name="nomecliente" type="text" class="form-control input-md" required>
                        </center>
                    </div>


                    <!-- CPF -->
                    <div class="form-group form-inline">
                        <center>
                            <!-- RG -->
                            <label  for="rg">RG</label> 
                            <input id="rg" name="rg" type="text"  class="form-control input-md"
                                   style="width: 150px; margin-left: 10px;" required onkeyup="maskIt(this, event, '###.###.###', true)"/>
                            <div class="col-md-12" id="validarcpf"></div>
                            <label style="margin-left: 10px;" for="cpf">CPF</label>  
                            <input id="cpf" name="cpf" type="text" class="form-control input-md" 
                                   style="width: 180px; margin-left: 10px;" required onkeyup="maskIt(this, event, '###.###.###-##', true)" 
                                   onchange="validarcpf(this.value)"/>
                            <label  style="margin-left: 10px;" for="datanasc">Data de nascimento</label> 
                            <input id="textinput" name="datanasc" type="date" class="form-control input-md" style="width: 160px;" required/>

                        </center>
                    </div>

                    <!-- Email -->
                    <div class="form-group form-inline">
                        <center>
                            <label for="email">E-mail</label>  
                            <input style="width: 685px; margin-left: 10px;" id="textinput" name="email" 
                                   type="text" class="form-control input-md" style="width: 300px;" required/>
                        </center>
                    </div>
                </div>

                <!-- telefone -->
                <div style="padding-top: 30px">
                    <fieldset>
                        <legend style="text-align: center">Telefone</legend>
                        <a href="#" id="add" name="add" class="btn btn-primary" onclick="adicionarTelefone();"
                           style="text-align: center; margin-left: 25%;">+ Adicionar</a>
                        <div  id="telefone"></div>
                    </fieldset>
                </div>

                <div style="padding-top: 30px">
                    <fieldset>
                        <legend style="text-align: center">Endereço</legend>

                        <div class="form-group form-inline">
                            <center>
                                <label  for="log">Logradouro</label>  
                                <input style="width: 400px; margin: 5px;" id="log" name="log" type="text" class="form-control input-md" required/>
                                <label for="bairro">Bairro</label>
                                <input style="width: 400px; margin: 5px;" id="bairro" name="bairro" type="text" class="form-control input-md" required/>
                            </center>
                        </div>

                        <div class="form-group form-inline">
                            <center>
                                <label  for="complemento">Complemento</label>  
                                <input style="width: 850px; margin: 5px;" id="complemento" name="complemento" type="text" 
                                       class="form-control input-md" required/>
                            </center>
                        </div>
                        <div class="form-group form-inline">
                            <center>
                                <label for="cep">CEP</label> 
                                <input id="cep" name="cep" type="text" class="form-control input-md cep" required/>
                                <label  for="uf">UF</label>
                                <select id="uf" name="uf" class="form-control" required style="width: 110px;" onchange="showCidades(this.value)" required>
                                    <option value="">Selecione...</option>
                                    <?php
                                    $queryUf = $link->prepare("SELECT pk_uf, estado FROM tb_uf;");
                                    $queryUf->execute();

                                    $row = $queryUf->setFetchMode(PDO::FETCH_ASSOC);
                                    while ($row = $queryUf->fetch()) {
                                        $id = $row["pk_uf"];
                                        $estado = $row["pk_uf"];
                                        echo "<option value='$id'>$estado</option>";
                                    }
                                    ?>
                                </select>
                                <div style=" margin-right: 15px;"  id="selectCidade" name="selectCidade" class="form-group form-inline"> </div>
                            </center>
                        </div>
                    </fieldset>  
                </div>
                <div class="form-group">
                    <div class="col-md-5">&nbsp;</div>
                    <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Cadastrar"/>

                    <a href="../cliente/cliente_lista.php"><input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/></a>
                </div>
            </form>
        </div>

        <div style="display: none;" id="telNone">
            <div class="form-group form-inline" style="margin: 10px;"  >
                <div class="col-md-12" style="text-align: right;">
                    <center>
                        <label for="telcliente">Tipo</label> 
                        <select style=" margin-right: 15px;"  id="selectTipo" name="tipo[]" class="form-control" required>
                            <?php
                            $queryTipo = $link->prepare("SELECT pk_tipo_telefone, tipo FROM tb_tipo_telefone ;");
                            $queryTipo->execute();

                            $row = $queryTipo->setFetchMode(PDO::FETCH_ASSOC);
                            while ($row = $queryTipo->fetch()) {
                                $id = $row["pk_tipo_telefone"];
                                $tipo = $row["tipo"];
                                echo "<option value='$id'>$tipo</option>";
                            }
                            ?>
                        </select>
                        <label class="control-label" for="ddd">DDD</label>  
                        <input style=" margin-right: 15px;"  id="selectbasic" name="ddd[]" maxlength="2" size="2" class="form-control ddd">
                        <label class="control-label" for="numTelefone">Telefone</label>  
                        <input  style=" margin-right: 15px;"  id="numTel" name="numTelefone[]" type="text" class="form-control phone" required>
                        <a href="#" onclick="removerTelefone($(this).parent().parent().attr('id'));"><b>X</b></a>
                    </center>
                </div> 
            </div>
        </div>


    </div>

</body>
</html>
