<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Controle de Funcionário</title>
        
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/DataTables-1.10.16/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    
    <script>
        $(document).ready(function() {

            $('#list').DataTable({
                language:{
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
            });
        });
        
        function showModal(str) {
          $("#modalForn").load("../../controller/Funcionario/visualizar_funcionario.php?id="+str );
        }

        
    </script>

    <body style="font-size: 12px">

	    <?php
            include_once '../../conectbd.php';
        $query=$link->prepare("SELECT pk_funcionario,nome_func,sexo,cpf,rg,estado_civil,fk_endereco,funcao FROM  tb_funcionario;");
        $query->execute();
        $row = $query->setFetchMode(PDO::FETCH_ASSOC);        
        ?>
        <?php include '../menu.php' ?>
        
        <div class="container">
            <h2 style="text-align: center">Lista de Funcionários</h2>
            <div style="padding: 10px;">
                <a href="funcionario_cadastro.php" class="btn btn-primary btnCenter">+ Adicionar</a>
            </div>
                <table id="list" class="table table-hover" width="100%">
                    <thead>
                        <tr>    
                            <th>Funcionário</th>
                            <th>CPF</th>
                            <th>RG</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($row=$query->fetch()){
                            $id     =$row["pk_funcionario"];
                            $func   =$row["nome_func"];
                            $cpf    =$row["cpf"];
                            $rg     =$row["rg"];
                        echo "<tr>";
                        echo "<td>".$func."</td>";
                        echo"<td>".$cpf."</td>";
                        echo"<td>".$rg."</td>";
                        echo '<td><a onclick="showModal('. $id .')" data-toggle="modal" data-target="#myModal">Ver mais...</a> | <a href="../../controller/Funcionario/exclusao_funcionario.php?id=' . $row["pk_funcionario"] . '">Remover</a> |'
                        . ' <a href="funcionario_alterar.php?id=' .urlencode($id). '&nome=' .urlencode($func). '&cpf=' .urlencode($cpf). '&rg=' .urlencode($rg).'">Editar</a></td>';
                        echo "</tr>";
                       }
                        ?>
                    </tbody>    
                </table>
        </div>
        
        
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Fornecedor</h4>
                    </div>
                    <div class="modal-body" id="modalForn">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
