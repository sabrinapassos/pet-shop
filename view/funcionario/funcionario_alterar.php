<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de Funcionário</title>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript">
            $(document).ready(function () {
                $('#cpf').mask('000.000.000-00', {reverse: true});
                $('#rg').mask('0.000.000', {reverse: true});
                $('#cep').mask('00000-000');
                $('.phone').mask('000000000');
                $('.ddd').mask('00');
            })
        </script>    
    </head>

    <script>
        var indexTel = 1;

        function removerTelefone(id) {
            $("#" + id).remove();
        }

        function adicionarTelefone() {
            $("#telNone").children()[0].id = "telefone" + indexTel++;
            var telDiv = $("#telNone").html();
            $("#telefone").append(telDiv);
        }

        function showCidades(str) {
            $("#selectCidade").load("../../controller/cidades.php?uf=" + str);
        }


    </script>

    <body style="font-size: 12px">

        <?php
        include '../menu.php';
        include '../../conectbd.php';

        global $id;
        global $row;
        global $idfunc;
        global $nome;
        global $sexo;
        global $cpf;
        global $rg;
        global $ec;
        global $funcao;
        global $logradouro;
        global $complemento;
        global $bairro;
        global $cep;
        global $cidade;
        global $uf;
        global $idUf;

        if (!empty($_GET["id"])) {
            $id = $_GET["id"];

            $query = $link->prepare("select f.pk_funcionario, f.nome_func, f.sexo, f.cpf, f.rg, f.estado_civil, f.funcao, e.logradouro, e.complemento, e.bairro, e.cep ,
                            c.nome, c.pk_cidade, uf.estado, e.pk_endereco from tb_funcionario f 
                            inner join tb_endereco e on f.fk_endereco = e.pk_endereco
                            inner join tb_cidade c on c.pk_cidade = e.fk_cidade
                            inner join tb_uf uf on uf.pk_uf = c.fk_uf
                            where f.pk_funcionario = '" . $id . "'");
            $query->execute();
            $row = $query->fetch();
            $idfunc = $row["pk_funcionario"];
            $nome = $row["nome_func"];
            $sexo = $row["sexo"];
            $cpf = $row["cpf"];
            $rg = $row["rg"];
            $ec = $row["estado_civil"];
            $funcao = $row["funcao"];
            $logradouro = $row["logradouro"];
            $complemento = $row["complemento"];
            $bairro = $row["bairro"];
            $cep = $row["cep"];
            $cidade = $row["pk_cidade"];
            $uf = $row["estado"];
            $end = $row["pk_endereco"];
        }
        ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/Funcionario/alteracao_funcionario.php">
                    <input  name="id" type="hidden"  value="<?php echo $id; ?>">
                    <input  name="endereco" type="hidden"   value="<?php echo $end; ?>">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Funcionário</h2>

                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                            <label for="nome">Funcionário</label>  
                            <input style="width: 400px; margin: 5px;" id="textinput"  maxlength="50" name="nome" type="text"  value="<?php echo $nome; ?>" class="form-control input-md">

                            <label for="funcao">Função</label>
                            <select style="width: 400px; margin: 5px;" id="funcao" name="funcao" class="form-control" required>
                             <option>Selecione</option>
                             <option value="Administrador" <?= ($funcao == 'Administrador') ? 'selected' : '' ?>>Administrador</option>
                             <option value="Vendedor" <?= ($funcao == 'Vendedor') ? 'selected' : '' ?>>>Vendedor</option>
                             <option value="Banhista" <?= ($funcao == 'Banhista') ? 'selected' : '' ?>>Banhista</option>
                             <option value="tosador" <?= ($funcao == 'tosador') ? 'selected' : '' ?>>Tosador </option>
                             <option value="Veterinário" <?= ($funcao == 'Veterinário') ? 'selected' : '' ?>>Veterinário</option>
                             <option value="Recepcionista" <?= ($funcao == 'Recepcionista') ? 'selected' : '' ?>>Recepcionista</option>
                        </select>
                        </center> 
                    </div>



                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                            <label for="cpf">CPF</label>  
                            <input style="width: 400px; margin: 5px;" id="cpf" name="cpf" type="text"  value="<?php echo $cpf; ?>" class="form-control input-md" onkeyup="maskIt(this, event, '###.###.###-##', true)" />

                            <label for="rg">RG </label>
                            <input style="width: 400px; margin: 5px;" id="rg" name="rg" type="text"  value="<?php echo $rg; ?>" class="form-control input-md" onkeyup="maskIt(this, event, '###.###.###', true)" />
                        </center>
                    </div>



                    <div class="form-group form-inline">
                        <center>
                            <label for="sexo">Sexo</label>  
                            <select style="width: 350px; margin: 5px;" id="sexo" name="sexo" class="form-control">
                                <option>Selecione</option>
                                <option value="m" <?= ($sexo == 'm') ? 'selected' : '' ?> >Masculino</option>
                                <option value="f" <?= ($sexo == 'f') ? 'selected' : '' ?>>Feminino</option>
                            </select>

                            <label for="estado_civil">Estado Civil</label>
                            <select style="width: 350px; margin: 5px;" id="estado_civil" name="estado_civil" class="form-control">
                                <option>Selecione</option>
                                <option value="Casado" <?= ($ec == 'Casado') ? 'selected' : '' ?> >Casado </option>
                                <option value="Solteiro" <?= ($ec == 'Solteiro') ? 'selected' : '' ?>>Solteiro</option>
                                <option value="Separado" <?= ($ec == 'Separado') ? 'selected' : '' ?>>Separado(a) </option>
                                <option value="Divorciado" <?= ($ec == 'Divorciado') ? 'selected' : '' ?> >Divorciado(a) </option>
                                <option value="Viuvo" <?= ($ec == 'Viuvo') ? 'selected' : '' ?>>Viúvo(a)</option>
                        
                            </select>
                        </center>
                    </div>



                    <div style="padding-top: 30px">
                        <fieldset>
                            <legend style="text-align: center">Telefone</legend>

                            <!-- Select Basic -->

                            <a href="#" id="add" name="add" class="btn btn-primary" onclick="adicionarTelefone();"
                               style="text-align: center; margin-left: 25%;">+ Adicionar</a>
                            <div  id="telefone">
                                 <?php
                                
                                if (!empty($id)) {
                                    $queryTel = $link->prepare("select tp.tipo, f.ddd, f.telefone from tb_telefone_funcionario  f inner join tb_tipo_telefone tp on  f.fk_tipo = tp.pk_tipo_telefone where fk_funcionario = '" . $id . "'");
                                    $queryTel->execute();

                                    echo "<div class='form-group form-inline' style='margin: 10px;'  >";
                                        while ($rowTel = $queryTel->fetch()) {
                                            echo " <label class='col-md-4 control-label' for='telfuncionario'>Tipo</label>"
                                            . " <div class='col-md-8' style='text-align: left; padding-bottom: 10px;'>"
                                            . " <select style=' margin-right: 15px;'  id='selectTipo' name='tipo[]' class='form-control' required>";

                                            $queryTipo = $link->prepare("SELECT pk_tipo_telefone, tipo FROM tb_tipo_telefone;  ");
                                            $queryTipo->execute();

                                            $row = $queryTipo->setFetchMode(PDO::FETCH_ASSOC);
                                            while ($row = $queryTipo->fetch()) {
                                                $id = $row["pk_tipo_telefone"];
                                                $tipo = $row["tipo"];
                                                if ($tipo == $rowTel['tipo']) {
                                                    echo "<option value = '$id' selected>$tipo</option>";
                                                } else {
                                                    echo "<option value = '$id'>$tipo</option>";
                                                }
                                            }

                                            echo ' </select> 
                                                    <label class="control-label" for="ddd">DDD</label> 
                                                    <input style=" margin-right: 15px; "  id="selectbasic" name="ddd[]" maxlength="2" size="2" 
                                                    class="form-control ddd" value="' . $rowTel["ddd"] . '">
                                                    <label class="control-label" for="numTelefone">Telefone</label>  
                                                    <input  style=" margin-right: 15px;"  id="numTel" name="numTelefone[]" type="text" 
                                                    class="form-control phone" value="' . $rowTel["telefone"] . '" required>
                                                    <a href="#" onclick="removerTelefone($(this).parent().parent().attr(\'id\'));"><b>X</b></a></div>';
                                        }
                                    echo "</div>";
                                }
                                ?>

                            </div>
                        </fieldset>
                    </div>


                    <div style="padding-top: 30px"> 
                        <fildset>   
                            <legend style="text-align: center">Endereço</legend>


                            <div class="form-group form-inline">
                                <center>
                                    <label for="log">Logradouro</label>  
                                    <input style="width: 400px; margin: 5px;" id="log" name="log" type="text" value="<?php echo"$logradouro" ?>" class="form-control input-md">

                                    <label for="bairro">Bairro</label>
                                    <input style="width: 400px; margin: 5px;" id="bairro" name="bairro" type="text"  value="<?php echo"$bairro" ?>" class="form-control input-md">
                                </center>
                            </div>


                            <div class="form-group form-inline">
                                <center>
                                    <label for="complemento">Complemento</label>  
                                    <input style="width: 850px; margin: 5px;" id="complemento" maxlength="100" name="complemento" type="text" value="<?php echo"$complemento" ?>" class="form-control input-md">
                                </center>
                            </div>

                            <div class="form-group form-inline">
                                <center>
                                    <label for="cep">CEP</label>   
                                    <input style="width: 200px; margin: 15px;" id="cep" name="cep" type="text" value="<?php echo"$cep" ?>" class="cep form-control input-md">    

                                    <label for="uf">UF</label>
                                    <select style="width: 200px; margin: 15px;" id="uf" name="uf" class="form-control" onchange="showCidades(this.value)">
                                        <option value="0">Selecione...</option>
                                        <?php
                                        $queryUf = $link->prepare("SELECT pk_uf, estado FROM tb_uf;");
                                        $queryUf->execute();

                                        $row = $queryUf->setFetchMode(PDO::FETCH_ASSOC);
                                        while ($row = $queryUf->fetch()) {
                                            $id = $row["pk_uf"];
                                            $estado = $row["estado"];

                                            if ($uf == $estado) {
                                                echo "<option value='$id' selected>$estado</option>";
                                                $idUf = $id;
                                            } else {
                                                echo "<option value='$id'>$estado</option>";
                                            }
                                        }
                                        ?>
                                    </select>

                                    <div style=" margin-right: 15px;"  id="selectCidade" name="selectCidade" class="form-group form-inline">

                                    </div>
                                </center>
                            </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-5">&nbsp;</div>
                        <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Cadastrar"/>

                        <input id="cancelar" name="cancelar" type="button" onclick="window.open('funcionario_lista.php');" class="btn btn-primary btnCenter" value="Cancelar"/>
                    </div>


            </div>

        </fieldset>
    </form>
</div>
</div>

<div style="display: none;" id="telNone">
    <div class="form-group form-inline" style="margin: 10px;"  >
        <label class="col-md-4 control-label" for="telfuncionario">Tipo</label> 
        <div class="col-md-8" style="text-align: left;">
            <select style=" margin-right: 15px;"  id="selectTipo" name="tipo[]" class="form-control" required>
                <?php
                $queryTipo = $link->prepare("SELECT pk_tipo_telefone, tipo FROM tb_tipo_telefone ;");
                $queryTipo->execute();

                $row = $queryTipo->setFetchMode(PDO::FETCH_ASSOC);
                while ($row = $queryTipo->fetch()) {
                    $id = $row["pk_tipo_telefone"];
                    $tipo = $row["tipo"];
                    echo "<option value='$id'>$tipo</option>";
                }
                ?>
            </select>
            <label class="control-label" for="ddd">DDD</label>  
            <input style=" margin-right: 15px;"  id="selectbasic" name="ddd[]" maxlength="2" size="2" class="form-control ddd">
            <label class="control-label" for="numTelefone">Telefone</label>  
            <input  style=" margin-right: 15px;"  id="numTel" name="numTelefone[]" type="text" class="form-control phone" required>
            <a href="#" onclick="removerTelefone($(this).parent().parent().attr('id'));"><b>X</b></a>
        </div> 
    </div>
</div>

</body>
</html>

<?php
if ($idUf != null) {
    echo " <script> showCidades('$idUf', '$cidade'); </script>";
}
?>


