<?php
session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
$id = urldecode($_GET["id"]);
$pet = urldecode($_GET["nome"]);
$raca = urldecode($_GET["raca"]);
$sexo = urldecode($_GET["sexo"]);
$data = urldecode($_GET["dt_nascimento"]);
$porte = urldecode($_GET["porte"]);
$obs = urldecode($_GET["observacoes"]);
$dono = urldecode($_GET["dono"]);
?>

<html>
    <head>
        <title>Cadastro de pet</title>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <script>
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
    </script> 

    <body class="body" style="font-size: 12px">

        <?php include '../menu.php' ?>
        <?php include '../../conectbd.php' ?>



        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/Pet/alteracao-pet.php" method="GET">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Cadastro de pets</h2>

                    <!-- Nome -->
                    <div class="form-group form-inline">

                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <center>
                            <label for="nomepet">Nome</label>  
                            <input id="textinput" name="nomepet" type="text" class="form-control input-md" value="<?php echo $pet; ?>" style="width: 800px;"  required/>
                        </center>

                    </div>


                    <!-- raça -->
                    <div class="form-group form-inline">
                        <center>
                            <label for="raça">Raça</label>  
                            <input id="textinput" name="raca" type="text" class="form-control input-md" value="<?php echo $raca; ?>" style="width: 170px; margin-left: 10px;" required/>
                            <label for="sexo" style="margin-left: 5px;">Sexo</label>
                            <select id="sexo" name="sexo" class="form-control" style="width: 120px; margin-left: 10px;" required/>
                            <option value="Feminino" <?= ($sexo == 'Feminino') ? 'selected' : '' ?> >Feminino </option>
                            <option value="Masculino" <?= ($sexo == 'Masculino') ? 'selected' : '' ?>>Masculino</option>
                            </select>
                            <label for="porte" style="margin-left: 5px;">Porte</label>
                            <select id="porte" name="porte" class="form-control" style="width: 120px; margin-left: 10px;" required/>
                            <option value="Pequeno" <?= ($porte == 'Pequeno') ? 'selected' : '' ?>  >Pequeno</option>
                            <option value="Medio" <?= ($porte == 'Medio') ? 'selected' : '' ?> >Médio</option>
                            <option value="Grande" <?= ($porte == 'Grande') ? 'selected' : '' ?> >Grande</option>
                            </select>

                            <label  for="datanasc">Data nascimento</label>
                            <input id="textinput" name="data" type="date" class="form-control input-md" value="<?php echo $data; ?>" style="width: 150px; margin-left: 10px;" required/>
                        </center>
                    </div>
                    <div class="form-group form-inline">
                        <center>
                            <label for="dono">Dono</label>
                            <select  name="dono" class="form-control" style="width: 600px; margin-left: 10px;" required>
                                <option value="">Selecione...</option>
                                <?php
                                $query = $link->prepare("SELECT pk_cliente, nome_cliente FROM tb_cliente");
                                $query->execute();

                                $row = $query->setFetchMode(PDO::FETCH_ASSOC);
                                while ($row = $query->fetch()) {
                                    $id = $row["pk_cliente"];
                                    $nomeCliente = $row["nome_cliente"];

                                    if ($id == $dono) {
                                        echo "<option value='$id' selected> $id - $nomeCliente </option>";
                                    } else {
                                        echo "<option value='$id'> $id - $nomeCliente </option>";
                                    }
                                }
                                ?>
                            </select>
                        </center>
                    </div>


                    <div class="form-group form-inline">
                        <label class="col-md-4 control-label" for="obs">Observações</label>
                        <textarea id="textinput" name="obs" class="form-control col-lg-10" rows="5" cols="50" maxlength="500"><?php echo $obs ?></textarea>

                    </div>

                    <div class="form-group">
                        <div class="col-md-5">&nbsp;</div>
                        <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Salvar"/>

                        <a href="pet_lista.php"><input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/></a>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>