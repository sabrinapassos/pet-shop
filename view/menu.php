
<nav class="navbar navbar-default sidebar" role="navigation">
    <img class="logo" src="../../assets/images/buddy_pet.png"/>
  <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
         </button>      
       </div>
    <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
        
        <ul class="nav navbar-nav" style="padding-top: 20px;">
            <li><a href="../venda/venda_lista.php">Vendas &nbsp;
            <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-shopping-cart"></span></a>
        </li>
        <li><a href="../servico/servico_lista.php">Serviços &nbsp;
            <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-briefcase"></span></a>
        </li>  
        <li><a href="../ordem_servico/ordem_servico_lista.php">OS &nbsp;
            <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-list"></span></a>
        </li>  
        <li><a href="../produto/produto_lista.php">Produtos &nbsp;
            <span class="pull-right hidden-xs showopacity fa fa-shopping-bag" style="font-size: 20px;"></span></a>
        </li>
        <li><a href="../cliente/cliente_lista.php">Cliente &nbsp;
            <span style="font-size:16px;" class="pull-right hidden-xs showopacity  glyphicon glyphicon-user"></span></a>
        </li>
        <li><a href="../Pet/pet_lista.php">Pet &nbsp;
            <span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-paw"></span></a>
        </li> 
        <li><a href="../fornecedor/fornecedor-lista.php">Fornecedor &nbsp;
            <span class="pull-right hidden-xs showopacity fa fa-cart-arrow-down" style="font-size: 20px;"></span></a>
        </li>
        <li><a href="../compra/compra_lista.php">Compra &nbsp;
            <span class="pull-right hidden-xs showopacity fa fa-shopping-bag" style="font-size: 20px;"></span></a>
        </li>
        <li><a href="../Contas_a_pagar/contas_pagar_lista.php">Contas &nbsp;
            <span class="pull-right hidden-xs showopacity fa fa-shopping-bag" style="font-size: 20px;"></span></a>
        </li>
        <li><a href="../Funcionario/funcionario_lista.php">Funcionário &nbsp;
            <span class="pull-right hidden-xs showopacity fa fa-male" style="font-size: 20px;"></span></a>
        </li>
        <li><a href="../usuario/usuario_lista.php">Usuário &nbsp;
            <span class="pull-right hidden-xs showopacity fa fa-user" style="font-size: 20px;"></span></a>
        </li> 
		
        <li style="padding-left: 30px;"><a href="../../logout.php">Sair &nbsp;
            <i  class="pull-right hidden-xs showopacity glyphicon glyphicon-log-out" style="font-size: 16px;"></i></a>
        </li>
       
      </ul>
    </div>
  </div>
    <div style="text-align: right;">
        <?php 
            date_default_timezone_set('America/Sao_Paulo');
            $date = date('H:i d-m-Y');
            echo $date;
            
        ?>
    </div>
     
</nav>`



