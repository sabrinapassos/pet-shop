<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Lista de Fornecedor</title>

        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>

        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/DataTables-1.10.16/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <script>
        $(document).ready(function () {

            $('#list').DataTable({
                language: {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
        });

    </script>

    <body style="font-size: 12px">
        <?php
        include_once '../../conectbd.php';
        $query = $link->prepare("select os.fk_venda, os.fk_venda_servico, os.pk_ordem_servico, pet.nome_pet, cliente.nome_cliente, 
                                     servico.servico, DATE_FORMAT(venda.dt_venda, '%d/%m/%Y') dt_venda from tb_ordem_servico os
                                    inner join tb_venda venda on venda.pk_venda = os.fk_venda
                                    inner join tb_cliente cliente on cliente.pk_cliente = venda.fk_cliente
                                    inner join tb_venda_servico vs on vs.fk_venda = venda.pk_venda
                                    inner join tb_pet pet on pet.pk_pet = vs.fk_pet
                                    inner join tb_servico servico on servico.pk_servico = vs.fk_servico;");
        $query->execute();
        $row = $query->setFetchMode(PDO::FETCH_ASSOC);
        ?>

        <?php include '../menu.php' ?>

        <div class="container">
            <h2 style="text-align: center">Ordem de Serviço</h2>

            <table id="list" class="table table-hover" width="100%">
                <thead>
                    <tr>    
                        <th>Código</th>
                        <th>Venda</th>
                        <th>Data da venda</th>
                        <th>Serviço</th>
                        <th>Pet</th>
                        <th>Cliente</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($row = $query->fetch()) {
                        $idOS = $row["pk_ordem_servico"];
                        $idVenda = $row["fk_venda"];
                        $servico = $row["servico"];
                        $pet = $row["nome_pet"];
                        $cliente = $row["nome_cliente"];
                        $data = $row["dt_venda"];
                        echo "<tr>";
                        echo "<td>" . $idOS . "</td>";
                        echo "<td>" . $idVenda . "</td>";
                        echo "<td>" . $data . "</td>";
                        echo "<td>" . $servico . "</td>";
                        echo "<td>" . $pet . "</td>";
                        echo "<td>" . $cliente . "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>   
            </table>
        </div>
    </body>
</html>

