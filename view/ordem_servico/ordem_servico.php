<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de OS</title>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    </head>

    <body style="font-size: 12px">

        <?php include '../menu.php' ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Ordem de Serviço</h2>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="pet">Pet</label>  
                        <div class="col-md-4">
                            <input id="textinput" name="pet" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="data">Data</label>  
                        <div class="col-md-4">                            
                            <input id="textinput" name="data" type="date" class="form-control input-md">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="duracao">Duração</label>  
                        <div class="col-md-4">
                            <input id="textinput" name="duracao" type="text" class="form-control input-md">
                        </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="horario">Horário</label>  
                        <div class="col-md-4">
                            <input id="textinput" name="horario" type="time" class="form-control input-md">
                        </div>
                    </div>
                    
                     <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="observacoes">Observações</label>  
                        <div class="col-md-4">
                            <textarea id="textinput" name="observacoes" rows="10" cols="40" class="form-control input-md"></textarea>
                        </div>
                    </div>
                    
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">&nbsp;</div>
                                <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Cadastrar"/>
                                
                                <input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/>
                            </div>


                    </div>

                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>

