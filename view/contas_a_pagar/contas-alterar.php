<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<html>
    <head>
        <title>Contas a pagar</title>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    
    <script type="text/javascript">
        $(document).ready(function () {
        $('.code').mask('00000.00000 00000.00000 00000.00000 0 00000000000000');
        $('.money').mask('0.000.000.000,00', {reverse: true});
        $('.phone').mask('0000-00000');
               
        })
        </script>

    <script>

        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('000000000');
            $('.ddd').mask('00');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
    </script>
    
    <body style="font-size: 12px">
        
        <?php
        include '../menu.php';
        include '../../conectbd.php';     

        
            $id = $_GET["id"];


            $query = $link->prepare
                    (" SELECT pk_conta_pagar, total_parc ,num_parc, nosso_num, dt_vencimento, vl_conta, cod_barra, dt_pag, vl_pag, fk_compra, fk_fornecedor
            FROM tb_conta_pagar
            WHERE pk_conta_pagar = '" . $id . "'");
            $query->execute();
            $row = $query->Fetch();

            //cliente        
            $idconta = $row["pk_conta_pagar"];
            $totalparc = $row["total_parc"];
            $numparc = $row["num_parc"];
            $nossonum = $row["nosso_num"];
            $dtvencimento = $row["dt_vencimento"];
            $vlconta = $row["vl_conta"];
            $code = $row["cod_barra"];
            $dtpag = $row["dt_pag"];
            $vlpag = $row["vl_pag"];
            $fkcompra = $row["fk_compra"];
            $fkfornecedor = $row["fk_fornecedor"];

        ?>       
    
    <div class="container">         
        <div class="row">           
        <div class="col-md-4"></div>
        <form class="form-horizontal" style="padding-bottom: 100px;" action="../../controller/contas_pagar/cadastro-contapagar.php" method="GET">
                <h1 style="text-align: center">Contas a pagar</h1>
                <fieldset>
                
                <div class="form-group ">
                    <input type="hidden" name="id" value="<?php echo"$id" ?>"/>
                <!-- Fornecedor-->
                <div class="form-group form-inline">
                       <center>
                           <label for="fornecedor">Fornecedor</label>
                          <select style="width: 800px; margin: 5px;" name="fornecedor" class="form-control" >
                               <option value="">Selecione...</option>
                               <?php
                              $query = $link->prepare("SELECT pk_fornecedor, razao_social FROM tb_fornecedor");
                              $query->execute();

                              $row = $query->setFetchMode(PDO::FETCH_ASSOC);
                              while ($row = $query->fetch()) {
                                  $id = $row["pk_fornecedor"];
                                  $razaosocial = $row["razao_social"];

                                  echo "<option value='$id' selected> $id - $razaosocial </option>";
                               }
                               ?>
                          </select>
                      </center>
                </div>
                <!-- compra-->     
                <div class="form-group form-inline">
                        <center>
                            <label for="compra">Compra</label>
                            <select style="width: 800px; margin: 5px;" name="compra" class="form-control" >
                                <option value="">Selecione...</option>
                                <?php
                                $query = $link->prepare("SELECT pk_compra, valor_compra FROM tb_compra");
                                $query->execute();

                                $row = $query->setFetchMode(PDO::FETCH_ASSOC);
                                while ($row = $query->fetch()) {
                                    $id = $row["pk_compra"];
                                    $valor = $row["valor_compra"];

                                    echo "<option value = '$id' selected>$id - $valor </option>";
                                }
                                ?>
                            </select>
                        </center>
                </div>
                        
		<!--codebarra--> 
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="codebarra">Código de barra</label>
                    <div class="col-md-4">
                        <input id="textinput" name="codebarra" type="number" class="code form-control input-md" value="<?php echo"$code" ?>" required>
                    </div>
                </div>           
					  
                <!--valorconta--> 
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="valorconta">Valor total da conta</label>
                    <div class="col-md-4">
                        <input id="textinput" name="valorconta" type="text" class="money form-control input-md" value="<?php echo"$vlconta" ?>" required>
                    </div>
                </div>
                
                <!--nossonumero-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nossonumero">Nosso número</label>
                    <div class="col-md-4">
                        <input id="textinput" name="nossonumero" type="number"  class="phone form-control input-md" value="<?php echo"$nossonum" ?>" required>
                    </div>
                </div>
                
                <!--totalparc-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="totalparc">Número total de parcelas</label>
                    <div class="col-md-4 ">
                        <select class="form-control col-md-4" name="totalparc">
                            <option value="">Selecione</option>                            
                            <option value="1"  <?= ($totalparc == '1') ? 'selected' : '' ?> >1x</option>
                            <option value="2"  <?= ($totalparc == '2') ? 'selected' : '' ?>>2x</option>
                            <option value="3"  <?= ($totalparc == '3') ? 'selected' : '' ?>>3x</option>
                            <option value="4"  <?= ($totalparc == '4') ? 'selected' : '' ?>>4x</option>
                            <option value="5"  <?= ($totalparc == '5') ? 'selected' : '' ?>>5x</option>
                            <option value="6"  <?= ($totalparc == '6') ? 'selected' : '' ?>>6x</option>
                            <option value="7"  <?= ($totalparc == '7') ? 'selected' : '' ?>>7x</option>
                            <option value="8"  <?= ($totalparc == '8') ? 'selected' : '' ?>>8x</option>
                            <option value="9"  <?= ($totalparc == '9') ? 'selected' : '' ?>>9x</option>
                            <option value="10" <?= ($totalparc == '10') ? 'selected' : '' ?>>10x</option>
                            <option value="11" <?= ($totalparc == '11') ? 'selected' : '' ?>>11x</option>
                            <option value="12" <?= ($totalparc == '12') ? 'selected' : '' ?>>12x</option>
                        </select>
                    </div>
                </div>
                      
                <!--numparc-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="numparc">Número da parcela</label>
                    <div class="col-md-4 ">
                        <select class="form-control col-md-4" name="numparc">
                            <option value="">Selecione</option>                            
                            <option value="1"  <?= ($numparc == '1') ? 'selected' : '' ?> >1x</option>
                            <option value="2"  <?= ($numparc == '2') ? 'selected' : '' ?>>2x</option>
                            <option value="3"  <?= ($numparc == '3') ? 'selected' : '' ?>>3x</option>
                            <option value="4"  <?= ($numparc == '4') ? 'selected' : '' ?>>4x</option>
                            <option value="5"  <?= ($numparc == '5') ? 'selected' : '' ?>>5x</option>
                            <option value="6"  <?= ($numparc == '6') ? 'selected' : '' ?>>6x</option>
                            <option value="7"  <?= ($numparc == '7') ? 'selected' : '' ?>>7x</option>
                            <option value="8"  <?= ($numparc == '8') ? 'selected' : '' ?>>8x</option>
                            <option value="9"  <?= ($numparc == '9') ? 'selected' : '' ?>>9x</option>
                            <option value="10" <?= ($numparc == '10') ? 'selected' : '' ?>>10x</option>
                            <option value="11" <?= ($numparc == '11') ? 'selected' : '' ?>>11x</option>
                            <option value="12" <?= ($numparc == '12') ? 'selected' : '' ?>>12x</option>
                        </select>
                    </div>
                </div>
                                
                <!--datadevencimento-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="datadevencimento">Data de vencimento</label>
                    <div class="col-md-4">
                        <input id="textinput" name="datadevencimento" type="date" class="form-control input-md" value="<?php echo"$dtvencimento" ?>" required>
                    </div>
                </div>
                                
                <!--valorpago--> 
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="valorpago">Valor pago</label>
                    <div class="col-md-4">
                        <input id="textinput" name="valorpago" type="text" class="money form-control input-md" value="<?php echo"$vlpag" ?>" required>
                    </div>
                </div>
                
                <!--datadepagamento-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="datadepagamento">Data de Pagamento</label>
                    <div class="col-md-4">
                        <input id="textinput" name="datadepagamento" type="date" class="form-control input-md" value="<?php echo"$dtpag" ?>" required>
                    </div>
                </div>
                
                   <div style="padding-top: 30x;">
                     
                     <div class="form-group">
                      <div class="col-md-5">&nbsp;</div>
                      <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Confirmar"/>          
                      <input id="cancelar" name="cancelar" type="button" onclick="window.open('contas_pagar_lista.php');" class="btn btn-primary btnCenter" value="Cancelar"/>
                     </div>       
                   </div>
     </fieldset>
     </div>           
    </form>
   </div>
  </div>
 </body>
</html>