<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>
<html>
    <head>
        <title>Contas a pagar</title>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <script type="text/javascript">
        $(document).ready(function () {
        $('.code').mask('00000.00000 00000.00000 00000.00000 0 00000000000000');
        $('.money').mask('0.000.000.000,00', {reverse: true});
        $('.phone').mask('0000-00000');
               
        })
        </script>
    
    <body style="font-size: 12px">

        <?php include_once '../menu.php' ?>
        <?php include_once '../../conectbd.php' ?>
    
    <div class="container">         
        <div class="row">           
        <div class="col-md-4"></div>
        <form class="form-horizontal" style="padding-bottom: 100px;" action="../../controller/contas_pagar/cadastro-contapagar.php" method="GET">
                <h1 style="text-align: center">Contas a pagar</h1>
                <fieldset>
                
                <div class="form-group ">

                <!-- Fornecedor-->
                <div class="form-group form-inline">
                       <center>
                           <label for="fornecedor">Fornecedor</label>
                           <select style="width: 500px; margin: 5px;" name="fornecedor" class="form-control" required>
                               <option value="">Selecione...</option>
                               <?php
                              $query = $link->prepare("SELECT pk_fornecedor, razao_social FROM tb_fornecedor");
                              $query->execute();

                              $row = $query->setFetchMode(PDO::FETCH_ASSOC);
                              while ($row = $query->fetch()) {
                                  $id = $row["pk_fornecedor"];
                                  $razaosocial = $row["razao_social"];

                                  echo "<option value='$id'> $id - $razaosocial </option>";
                               }
                               ?>
                          </select>
                      </center>
                </div>
                <!-- compra-->     
                <div class="form-group form-inline">
                        <center>
                            <label for="compra">Compra</label>
                            <select style="width: 500px; margin: 5px;" name="compra" class="form-control" required>
                                <option value="">Selecione...</option>
                                <?php
                                $query = $link->prepare("SELECT pk_compra, valor_compra FROM tb_compra");
                                $query->execute();

                                $row = $query->setFetchMode(PDO::FETCH_ASSOC);
                                while ($row = $query->fetch()) {
                                    $id = $row["pk_compra"];
                                    $valor = $row["valor_compra"];

                                    echo "<option value='$id'> $id - $valor </option>";
                                }
                                ?>
                            </select>
                        </center>
                </div>
                        
		<!--codebarra--> 
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="codebarra">Código de barra</label>
                    <div class="col-md-4">
                        <input id="code" name="codebarra" type="text"  class="code form-control input-md" >
                    </div>
                </div>           
					  
                <!--valorconta--> 
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="valorconta">Valor total da conta</label>
                    <div class="col-md-4">
                        <input id="textinput" name="valorconta" type="text" class="money form-control input-md" required>
                    </div>
                </div>
                
                <!--nossonumero-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nossonumero">Nosso número</label>
                    <div class="col-md-4">
                        <input id="textinput" name="nossonumero" type="text"  class="phone form-control input-md">
                    </div>
                </div>
                
                <!--totalparc-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="totalparc">Número total de parcelas</label>
                    <div class="col-md-4 ">
                        <select class="form-control col-md-4" name="totalparc">
                            <option value="0">Selecione</option>                            
                            <option value="1">1x</option>
                            <option value="2">2x</option>
                            <option value="3">3x</option>
                            <option value="4">4x</option>
                            <option value="5">5x</option>
                            <option value="6">6x</option>
                            <option value="7">7x</option>
                            <option value="8">8x</option>
                            <option value="9">9x</option>
                            <option value="10">10x</option>
                            <option value="11">11x</option>
                            <option value="12">12x</option>
                        </select>
                    </div>
                </div>
                      
                <!--numparc-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="numparc">Número da parcela</label>
                    <div class="col-md-4 ">
                        <select class="form-control col-md-4" name="numparc">
                            <option value="0">Selecione</option>                            
                            <option value="1">1x</option>
                            <option value="2">2x</option>
                            <option value="3">3x</option>
                            <option value="4">4x</option>
                            <option value="5">5x</option>
                            <option value="6">6x</option>
                            <option value="7">7x</option>
                            <option value="8">8x</option>
                            <option value="9">9x</option>
                            <option value="10">10x</option>
                            <option value="11">11x</option>
                            <option value="12">12x</option>
                        </select>
                    </div>
                </div>
                                
                <!--datadevencimento-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="datadevencimento">Data de vencimento</label>
                    <div class="col-md-4">
                        <input id="textinput" name="datadevencimento" type="date" class="form-control input-md">
                    </div>
                </div>
                                
                <!--valorpago--> 
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="valorpago">Valor pago</label>
                    <div class="col-md-4">
                        <input id="textinput" name="valorpago" type="text" class="money form-control input-md">
                    </div>
                </div>
                
                <!--datadepagamento-->     
                <div class="form-group ">
                    <label class="col-md-4 control-label" for="datadepagamento">Data de Pagamento</label>
                    <div class="col-md-4">
                        <input id="textinput" name="datadepagamento" type="date" class="form-control input-md">
                    </div>
                </div>
                
                   <div style="padding-top: 30x;">
                     
                     <div class="form-group">
                      <div class="col-md-5">&nbsp;</div>
                      <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Confirmar"/>          
                      <input id="cancelar" name="cancelar" type="button" onclick="window.open('contas_pagar_lista.php');" class="btn btn-primary btnCenter" value="Cancelar"/>
                     </div>       
                   </div>
     </fieldset>
     </div>           
    </form>
   </div>
  </div>
 </body>
</html>