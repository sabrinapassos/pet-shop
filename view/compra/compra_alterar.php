<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de Compra</title>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript">
            $(document).ready(function(){
                 $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
                 $('#money').mask('0.000.000.000,00', {reverse: true});
                 $('#money2').mask('0.000.000.000,00', {reverse: true});
                 $('#money3').mask('0.000.000.000,00', {reverse: true});
            })
        </script> 
    </head>

    <body style="font-size: 12px">

        <?php
        include '../menu.php';
        include '../../conectbd.php';

        global $id;
        global $row;
        global $idcompra;
        global $fornecedor;
        global $vl_compra;
        global $obs;
        global $vl_desc;
        global $vl_acre;
        global $produto;
        
        if (!empty($_GET["id"])) {
            $id = $_GET["id"];
            
            $query=$link->prepare("SELECT c.pk_compra,c.quantidade,c.valor_compra,c.observacoes,c.vl_desconto,c.vl_ascre,f.pk_fornecedor,f.razao_social,
                       p.pk_produto,p.descricao_prod FROM  tb_compra c
                       inner join tb_fornecedor f on c.fk_fornecedor = f.pk_fornecedor
                       inner join tb_produto p on c.fk_produto = p.pk_produto
                       where c.pk_compra = '".$id."'");
$query->execute();
$row = $query->fetch();
$idcompra = $row["pk_compra"];
$fornecedor = $row["razao_social"];
$vl_compra = $row["valor_compra"];
$quantidade = $row["quantidade"];
$obs = $row["observacoes"];
$vl_desc = $row["vl_desconto"];
$vl_acre = $row["vl_ascre"];
$produto = $row["descricao_prod"];
}
        ?>


        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/compra/alterar_compra.php">
                    <input  name="id" type="hidden"  value="<?php echo $id; ?>">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Cadastrar Compra</h2>
                    
                     <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                        <label for="fornecedor">Fornecedor</label>
                            <select style="width: 200px; margin: 5px;" name="fornecedor" class="form-control" >
                                <option value="">Selecione...</option>
                                <?php
                                $query = $link->prepare("SELECT pk_fornecedor, razao_social FROM tb_fornecedor");
                                $query->execute();

                                $row = $query->setFetchMode(PDO::FETCH_ASSOC);
                                while ($row = $query->fetch()) {
                                    $id = $row["pk_fornecedor"];
                                    $razaosocial = $row["razao_social"];
                                    if ($razaosocial == $rowdforn['tipo']) {
                                    echo "<option value = '$id' selected>$razaosocial</option>";
                                    } else {
                                    echo "<option value = '$id'>$razaosocial</option>";
                                                }

                                }
                                ?>
                            </select>
                        
                        <label for="produto">Produto</label>
                            <select style="width: 200px; margin: 5px;" name="produto" class="form-control" >
                                <option value="">Selecione...</option>
                                <?php
                                $queryprod = $link->prepare("SELECT pk_produto, descricao_prod FROM tb_produto");
                                $queryprod->execute();

                                $row = $queryprod->setFetchMode(PDO::FETCH_ASSOC);
                                while ($row = $queryprod->fetch()) {
                                    $idprod = $row["pk_produto"];
                                    $produto = $row["descricao_prod"];
                                    if ($produto == $rowprod['tipo']) {
                                    echo "<option value = '$id' selected>$produto</option>";
                                    } else {
                                    echo "<option value = '$id'>$produto</option>";
                                                }

                                }
                                ?>
                            </select>
                        
                        <label for="quantidade">Quantidade</label>  
                        <input style="width: 200px; margin: 5px;" maxlength="3" id="quantidade" name="quantidade" type="text" value="<?php echo $quantidade; ?>"  class="form-control input-md">
                        </center>
                    </div>
                    	
                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                        <label for="valor_compra">Valor da compra:</label>  
                        <input style="width: 200px; margin: 5px;" id="money" name="valor_compra" type="text" value="<?php echo $vl_compra; ?>"  class="form-control input-md">
                        
                        <label for="vl_desconto">Valor desconto:</label>
                        <input style="width: 200px; margin: 5px;" id="money2" name="vl_desconto" type="text" value="<?php echo $vl_desc; ?>"  class="form-control input-md"  size="20" />
                        
                        <label for="vl_ascre">Valor de acrescimo:</label> 
                        <input style="width: 200px; margin: 5px;" id="money3" name="vl_ascre" type="text" value="<?php echo $vl_acre; ?>"  class="form-control input-md"  size="20" />
                        </center>
                    </div>

                   
                     <!-- Text input-->
                    <div class="form-group form-inline">
                        <label class="col-md-4 control-label" for="observacoes">Observações</label>
                        <textarea id="textinput" name="observacoes" value="<?php echo $obs; ?>"  class="form-control col-lg-10" rows="3" cols="50" maxlength="500"></textarea>
                    </div>
                     
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">&nbsp;</div>
                                <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Alterar"/>
                                
                                <input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/>
                            </div>


                    </div>

                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>


