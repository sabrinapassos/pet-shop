<?php

session_start();
$login = $_SESSION['login'];
if (empty($login)) {
    header("Location: ../../index.html");
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de Compra</title>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript">
            $(document).ready(function () {
                $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
                $('#money').mask('0.000.000.000,00', {reverse: true});
                $('#money2').mask('0.000.000.000,00', {reverse: true});
                $('#money3').mask('0.000.000.000,00', {reverse: true});
            });

            function removeTableRow(linha) {
                $("#" + linha).remove();
            }

            function adicionarProduto() {

                var produtoId = $('#prod').val();
                if (produtoId != null && produtoId != "" && produtoId != 'undefined') {
                    var produtoTexto = $('#prod>option:selected').text();

                    var quantidade = parseFloat($('#qtd').val());
                    var vlUni = parseFloat($('#vlUni').val());



                    var newRow = $('<tr id="trProd' + produtoId + '">');
                    var cols = "";
                    cols += '<td>' + produtoId + '<input type="hidden" name="produtoId[]" value="' + produtoId + '"/></td>';
                    cols += '<td>' + produtoTexto + '<input type="hidden" name="produtoTexto[]" value="' + produtoTexto + '"/></td>';
                    cols += '<td>' + quantidade + '<input type="hidden" name="qtdProd[]" value="' + quantidade + '"/></td>';
                    cols += '<td>' + vlUni + '<input class="valor" type="hidden" name="vlUni[]" value="' + parseFloat(vlUni) + '"/></td>';
                    cols += '<td>';
                    cols += '<a onclick="removeTableRow(\'trProd' + produtoId + '\')" type="button">Remover</a>';
                    cols += '</td>';
                    newRow.append(cols);
                    $("#prodTable").append(newRow);

                }

            }


        </script> 
    </head>

    <body style="font-size: 12px">

        <?php
        include '../menu.php';
        include '../../conectbd.php';
        ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/compra/compra_cadastro.php">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Cadastrar Compra</h2>

                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                            <label for="fornecedor">Fornecedor</label>
                            <select style="width: 800px; margin: 5px;" name="fornecedor" class="form-control" >
                                <option value="">Selecione...</option>
                                <?php
                                $query = $link->prepare("SELECT pk_fornecedor, razao_social FROM tb_fornecedor");
                                $query->execute();

                                $row = $query->setFetchMode(PDO::FETCH_ASSOC);
                                while ($row = $query->fetch()) {
                                    $id = $row["pk_fornecedor"];
                                    $razaosocial = $row["razao_social"];

                                    echo "<option value='$id'> $id - $razaosocial </option>";
                                }
                                ?>
                            </select>
                        </center>
                    </div>


                    <div class="form-group">

                        <fieldset>
                            <legend style="text-align: center">Produtos</legend>
                            <div class="form-group form-inline">
                                <label style="padding: 10px;" for="prod">Produto </label>  
                                <select style="width:400px;"  id="prod" name="prod" class="form-control" >
                                    <option value="">Selecione...</option>
                                    <?php
                                    $queryProd = $link->prepare("SELECT pk_produto, descricao_prod, qtd_prod, preco_prod FROM tb_produto;");
                                    $queryProd->execute();

                                    $row = $queryProd->setFetchMode(PDO::FETCH_ASSOC);
                                    while ($row = $queryProd->fetch()) {
                                        $id = $row["pk_produto"];
                                        $descricao = $row["descricao_prod"];
                                        $preco = $row["preco_prod"];
                                        $qtd = $row["qtd_prod"];

                                        echo "<option data-preco='$preco' data-qtd='$qtd' value='$id'>$descricao</option>";
                                    }
                                    ?>
                                </select>
                                <label style="padding: 10px;" class="control-label" for="qtd"> Quantidade </label>  
                                <input style="width:50px;" id="qtd" name="qtd" type="number" class="form-control" value="1"/>

                                <label style="padding: 10px;" class="control-label" for="qtd"> Valor Unitario </label>  
                                <input style="width:100px;" id="vlUni" name="vlUni" type="number" class="form-control" value="1"/>
                                <a onclick="adicionarProduto();" class="btn btn-primary btnCenter">+ Adicionar</a>
                            </div>

                            <table id="prodTable" class="table table-over" width="100%">
                                <thead >
                                    <tr>  
                                        <th>Código</th>
                                        <th>Produto</th>
                                        <th>Quantidade</th>
                                        <th>Vl. Unitário</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody><tr></tr></tbody>    
                            </table>
                        </fieldset>
                    </div>

                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                            <label for="valor_compra">Valor da compra:</label>  
                            <input style="width: 200px; margin: 5px;" id="money" name="valor_compra" type="text" class="form-control input-md">
                        </center>
                    </div>

                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <label class="col-md-4 control-label" for="observacoes">Observações</label>
                        <textarea id="textinput" name="observacoes" class="form-control col-lg-10" rows="3" cols="50" maxlength="500"></textarea>
                    </div>

            </div>
            <div class="form-group">
                <div class="col-md-5">&nbsp;</div>
                <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Cadastrar"/>

                <input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/>
            </div>


        </div>

    </fieldset>
</form>
</div>
</div>
</body>
</html>


