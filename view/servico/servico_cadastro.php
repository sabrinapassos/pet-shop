<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>


<!DOCTYPE html>

<html>
    <head>
        <title>Cadastro de Serviços</title>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <script>
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('000000000');
            $('.ddd').mask('00');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
    </script>

    <body style="font-size: 12px">

        <?php include '../menu.php';
        include '../../conectbd.php';

        global $id;
        global $row;
        global $servico;
        global $preco;
        

        if (!empty($_GET["id"])) {
            $id = $_GET["id"];
            
            $query = $link->prepare("SELECT pk_servico, servico, preco FROM tb_servico where pk_servico = '" . $id . "'");

            $query->execute();
            $row = $query->fetch();
            $servico = $row["servico"];
            $preco = $row["preco"];
            
        }
        ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/servico/cadastro-servico.php">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Serviços</h2>

                    <!-- Text input-->
                    <center>
                        <div class="form-group form-inline">
                            <label for="servico">Serviço</label>  
                            <input style="width: 500px; margin: 20px;" id="textinput" name="servico" type="text" 
                                   class="form-control input-md" value="<?php echo"$servico" ?>">

                            <label class="control-label" for="preco">Preço </label>  
                            <input style="width: 150px; margin: 20px;" id="textinput" name="preco" type="text" class="form-control input-md money" 
                                    value="<?php echo"$preco" ?>"/>

                        </div>
                    </center>



            </div>
            <div class="form-group">
                <div class="col-md-5">&nbsp;</div>
                <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Cadastrar"/>

                <input id="cancelar" name="cancelar" type="button" onclick="window.open('servico_lista.php');" class="btn btn-primary btnCenter" value="Cancelar"/>
            </div>


        </div>

    </fieldset>
</form>
</div>
</div>
</body>
</html>

