
<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de Fornecedor</title>

        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <script>

        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('000000000');
            $('.ddd').mask('00');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });




        var indexTel = 1;

        function removerTelefone(id) {
            $("#" + id).remove();
        }

        function adicionarTelefone() {
            $("#telNone").children()[0].id = "telefone" + indexTel++;
            var telDiv = $("#telNone").html();
            $("#telefone").append(telDiv);
        }

        function showCidades(idUf, cidade) {
            $("#selectCidade").load("../../controller/cidades.php?uf=" + idUf + "&cidade=" + cidade);
        }

    </script>

    <body style="font-size: 12px">

        <?php
        include '../menu.php';
        include '../../conectbd.php';

        global $id;
        global $row;
        global $idforn;
        global $rz;
        global $nf;
        global $cnpj;
        global $logradouro;
        global $complemento;
        global $bairro;
        global $cep;
        global $cidade;
        global $uf;
        global $idUf;

        if (!empty($_GET["id"])) {
            $id = $_GET["id"];

            $query = $link->prepare("select f.pk_fornecedor, f.razao_social, f.nome_fantasia, f.cnpj, e.logradouro, e.complemento, e.bairro, e.cep ,
                            c.nome, c.pk_cidade, uf.estado from tb_fornecedor f 
                            inner join tb_endereco e on f.fk_endereco = e.pk_endereco
                            inner join tb_cidade c on c.pk_cidade = e.fk_cidade
                            inner join tb_uf uf on uf.pk_uf = c.fk_uf
                        where f.pk_fornecedor = '" . $id . "'");

            $query->execute();
            $row = $query->fetch();
            $idforn = $row["pk_fornecedor"];
            $rz = $row["razao_social"];
            $nf = $row["nome_fantasia"];
            $cnpj = $row["cnpj"];
            $logradouro = $row["logradouro"];
            $complemento = $row["complemento"];
            $bairro = $row["bairro"];
            $cep = $row["cep"];
            $cidade = $row["pk_cidade"];
            $uf = $row["estado"];
        }
        ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/fornecedor/cadastro-fornec.php">
                    <input type="hidden" name="id" value="<?php echo"$id" ?>"/>
                    <!-- Form Name -->
                    <h2 style="text-align: center">Cadastro de Fornecedor</h2>

                    <!-- Text input-->

                    <div class="form-group form-inline" style="margin: 20px;">
                        <center>
                            <label for="rzSocial">Razão Social</label>  
                            <input style="width: 850px; margin: 5px;" id="textinput" maxlength="100" name="rzSocial" type="text" class="form-control input-md" value="<?php echo"$rz" ?>" required>
                        </center>
                    </div>

                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                            <label  for="nmFantasia">Nome Fantasia</label>  
                            <input style="width: 400px; margin: 5px;" id="textinput" maxlength="100" name="nmFantasia" type="text" class="form-control input-md" value="<?php echo"$nf" ?>" required>

                            <label  for="cnpj">CNPJ</label>
                            <input style="width: 400px; margin: 5px;" class="cnpj form-control input-md" id="textinput" name="cnpj" type="text" value="<?php echo"$cnpj" ?>" required>
                        </center>

                    </div>

                    <div style="padding-top: 30px">
                        <fieldset>
                            <legend style="text-align: center">Telefone</legend>

                            <!-- Select Basic -->
                            <a href="#" id="add" name="add" class="btn btn-primary" onclick="adicionarTelefone();"
                               style="text-align: center; margin-left: 25%;">+ Adicionar</a>
                            <div  id="telefone">
                                <?php
                                
                                if (!empty($id)) {
                                    $queryTel = $link->prepare("select tp.tipo, f.ddd, f.telefone from tb_telefone_fornecedor  f inner join tb_tipo_telefone tp on  f.fk_tipo = tp.pk_tipo_telefone where fk_fornecedor = '" . $id . "'");
                                    $queryTel->execute();

                                    echo "<div class='form-group form-inline' style='margin: 10px;'  >";
                                        while ($rowTel = $queryTel->fetch()) {
                                            echo " <label class='col-md-4 control-label' for='telFornecedor'>Tipo</label>"
                                            . " <div class='col-md-8' style='text-align: left; padding-bottom: 10px;'>"
                                            . " <select style=' margin-right: 15px;'  id='selectTipo' name='tipo[]' class='form-control' required>";

                                            $queryTipo = $link->prepare("SELECT pk_tipo_telefone, tipo FROM tb_tipo_telefone;  ");
                                            $queryTipo->execute();

                                            $row = $queryTipo->setFetchMode(PDO::FETCH_ASSOC);
                                            while ($row = $queryTipo->fetch()) {
                                                $id = $row["pk_tipo_telefone"];
                                                $tipo = $row["tipo"];
                                                if ($tipo == $rowTel['tipo']) {
                                                    echo "<option value = '$id' selected>$tipo</option>";
                                                } else {
                                                    echo "<option value = '$id'>$tipo</option>";
                                                }
                                            }

                                            echo ' </select> 
                                                    <label class="control-label" for="ddd">DDD</label> 
                                                    <input style=" margin-right: 15px; "  id="selectbasic" name="ddd[]" maxlength="2" size="2" 
                                                    class="form-control ddd" value="' . $rowTel["ddd"] . '">
                                                    <label class="control-label" for="numTelefone">Telefone</label>  
                                                    <input  style=" margin-right: 15px;"  id="numTel" name="numTelefone[]" type="text" 
                                                    class="form-control phone" value="' . $rowTel["telefone"] . '" required>
                                                    <a href="#" onclick="removerTelefone($(this).parent().parent().attr(\'id\'));"><b>X</b></a></div>';
                                        }
                                    echo "</div>";
                                }
                                ?>
                            </div>
                        </fieldset>
                    </div>

                    <div style="padding-top: 30px">
                        <fieldset>
                            <legend style="text-align: center">Endereço</legend>


                            <div class="form-group form-inline">
                                <center>
                                    <label for="log">Logradouro</label> 
                                    <input style="width: 400px; margin: 5px;" id="log" maxlength="100" name="log" type="text" class="form-control input-md" value="<?php echo"$logradouro" ?>" required>

                                    <label for="bairro">Bairro</label>  
                                    <input style="width: 400px; margin: 5px;" id="bairro" maxlength="100" name="bairro" type="text" class="form-control input-md" value="<?php echo"$bairro" ?>" required>
                                </center>
                            </div>
                            <div class="form-group form-inline">
                                <center>
                                    <label for="complemento">Complemento</label> 
                                    <input style="width: 850px; margin: 5px;" id="complemento" name="complemento" type="text" class="form-control input-md" maxlength="100" value="<?php echo"$complemento" ?>" required>
                                </center>
                            </div>

                            <div class="form-group form-inline">
                                <center>
                                    <label for="cep">CEP</label>   
                                    <input style="width: 200px; margin: 15px;" id="complemento" name="cep" type="text" class="cep form-control input-md"  value="<?php echo"$cep" ?>" required>

                                    <label for="uf">UF</label>
                                    <select style="width: 200px; margin: 15px;" id="uf" name="uf" class="form-control" onchange="showCidades(this.value)" required>
                                        <option value="">Selecione...</option>
                                        <?php
                                        $queryUf = $link->prepare("SELECT pk_uf, estado FROM tb_uf;");
                                        $queryUf->execute();

                                        $row = $queryUf->setFetchMode(PDO::FETCH_ASSOC);
                                        while ($row = $queryUf->fetch()) {
                                            $id = $row["pk_uf"];
                                            $estado = $row["estado"];

                                            if ($uf == $estado) {
                                                echo "<option value='$id' selected>$estado</option>";
                                                $idUf = $id;
                                            } else {
                                                echo "<option value='$id'>$estado</option>";
                                            }
                                        }
                                        ?>
                                    </select>

                                    <div style=" margin-right: 15px;"  id="selectCidade" name="selectCidade" class="form-group form-inline">

                                    </div>
                                </center>
                            </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-5">&nbsp;</div>
                        <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Cadastrar"/>

                        <input id="cancelar" name="cancelar" type="button" onclick="window.open('fornecedor-lista.php');" class="btn btn-primary btnCenter" value="Cancelar"/>
                    </div>


            </div>
        </fieldset>
    </form>
</div>
</div>

<div style="display: none;" id="telNone">
    <div class="form-group form-inline" style="margin: 10px;"  >
        <label class="col-md-4 control-label" for="telFornecedor">Tipo</label> 
        <div class="col-md-8" style="text-align: left;">
            <select style=" margin-right: 15px;"  id="selectTipo" name="tipo[]" class="form-control" required>
                <?php
                $queryTipo = $link->prepare("SELECT pk_tipo_telefone, tipo FROM tb_tipo_telefone ;");
                $queryTipo->execute();

                $row = $queryTipo->setFetchMode(PDO::FETCH_ASSOC);
                while ($row = $queryTipo->fetch()) {
                    $id = $row["pk_tipo_telefone"];
                    $tipo = $row["tipo"];
                    echo "<option value='$id'>$tipo</option>";
                }
                ?>
            </select>
            <label class="control-label" for="ddd">DDD</label>  
            <input style=" margin-right: 15px;"  id="selectbasic" name="ddd[]" maxlength="2" size="2" class="form-control ddd">
            <label class="control-label" for="numTelefone">Telefone</label>  
            <input  style=" margin-right: 15px;"  id="numTel" name="numTelefone[]" type="text" class="form-control phone" required>
            <a href="#" onclick="removerTelefone($(this).parent().parent().attr('id'));"><b>X</b></a>
        </div> 
    </div>
</div>



</body>
</html>
<?php
if ($idUf != null) {
    echo " <script> showCidades('$idUf', '$cidade'); </script>";
}
?>
