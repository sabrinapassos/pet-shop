<?php 
session_start();
$login = $_SESSION['login'];
if(empty($login)){
    header("Location: ../../index.html");
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Cadastro de Usuário</title>
        <script src="../../assets/js/responsive-nav.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/js/jquery.mask.js" type="text/javascript"></script>
        <link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <script type="text/javascript">
            $(document).ready(function(){
                $('#cpf').mask('000.000.000-00', {reverse: true})
            })
        </script>    
    </head>
    
    <script>
    
     function validarcpf(str) {
            $("#validarcpf").load("../../controller/Funcionario/validar_cpf_funcinario.php?cpf=" + str);
        }
        
    
    </script>

    <body style="font-size: 12px">

       <?php include '../menu.php';
              include '../../conectbd.php';
        ?>

        <div class="container">

            <div class="row">
                <form class="form-horizontal" action="../../controller/Usuario/cadastro_usuario.php">

                    <!-- Form Name -->
                    <h2 style="text-align: center">Usuário</h2>

                   
                    <!-- Text input-->
                    <div class="form-group form-inline">
                        <center>
                        <label for="email">E-mail</label>  
                        <input style="width: 400px; margin: 5px;" id="email" name="email" type="text" class="form-control input-md">
                        
                   
                        <label for="senha">Senha</label>  
                        <input style="width: 200px; margin: 5px;" id="textinput" name="senha" type="password" class="form-control input-md" minlength="6" maxlength="20" required/>
                        </center>
                    </div>
                    
                   <div class="form-group form-inline"> 
                        <center>
                        <label for="login">Nome</label>  
                        <input style="width: 200px; margin: 5px;" id="nome" name="nome" type="text" class="form-control input-md">
                        
                        <label for="cpf">CPF</label>  
                        <input style="width: 180px; margin: 5px;" id="cpf" name="cpf" type="text" 
                               class="form-control input-md" required onkeyup="maskIt(this,event,'###.###.###-##',true)" 
                               onchange="validarcpf(this.value)"/>
                        <label for="dtnasc">Data de Nascimento</label>  
                        <input style="width: 160px; margin: 5px;" id="dtnasc" name="dtnasc" type="date" class="form-control input-md">  
                        <div class="col-md-13" id="validarcpf"></div> 
                        </center>
                    </div>
                    
                   
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">&nbsp;</div>
                                <input id="submit" name="submit" type="submit" class="btn btn-primary btnCenter" value="Cadastrar"/>
                                
                                <input id="cancelar" name="cancelar" type="button" class="btn btn-primary btnCenter" value="Cancelar"/>
                            </div>


                    </div>

                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>

